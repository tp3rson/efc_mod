package com.flansmod.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 30.06.2018
 * Time: 19:37
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class GuiUtils {
    private static final ResourceLocation beaconBeam = new ResourceLocation("textures/entity/beacon_beam.png");
    public static int gameColor = 16777215;
    public static boolean dM = true;

    public static void drawScissorBox(int x, int y, int width, int height) {
        GL11.glEnable((int) 3089);
        GL11.glScissor((int) x, (int) y, (int) width, (int) height);
    }

    public static void disableScissorBox() {
        GL11.glDisable((int) 3089);
    }

    public static void renderCenteredText(String text, int posX, int posY) {
        GuiUtils.renderCenteredText(text, posX, posY, gameColor);
    }

    public static void renderCenteredTextWithShadow(String text, int posX, int posY) {
        GuiUtils.renderCenteredTextWithShadow(text, posX, posY, gameColor);
    }

    public static void renderCenteredText(String text, int posX, int posY, int par4) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.fontRenderer.drawString(text, posX - mc.fontRenderer.getStringWidth(text) / 2, posY, par4);
    }

    public static void renderCenteredTextWithShadow(String text, int posX, int posY, int par4) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.fontRenderer.drawStringWithShadow(text, posX - mc.fontRenderer.getStringWidth(text) / 2, posY, par4);
    }

    public static void renderText(String text, int posX, int posY) {
        GuiUtils.renderText(text, posX, posY, gameColor);
    }

    public static void renderTextWithShadow(String text, int posX, int posY) {
        GuiUtils.renderTextWithShadow(text, posX, posY, gameColor);
    }

    public static void renderSplitText(String text, int posX, int posY, int wrapWidth) {
        GuiUtils.renderSplitText(text, posX, posY, wrapWidth, gameColor);
    }

    public static void renderText(String text, int posX, int posY, int color) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.fontRenderer.drawString(text, posX, posY, color);
    }

    public static void drawTextWithOutline(String text, int x, int y, int color) {
        Minecraft mc = Minecraft.getMinecraft();
        FontRenderer fr = mc.fontRenderer;
        fr.drawString(text, x - 1, y + 1, 0);
        fr.drawString(text, x, y + 1, 0);
        fr.drawString(text, x + 1, y + 1, 0);
        fr.drawString(text, x - 1, y, 0);
        fr.drawString(text, x + 1, y, 0);
        fr.drawString(text, x - 1, y - 1, 0);
        fr.drawString(text, x, y - 1, 0);
        fr.drawString(text, x + 1, y - 1, 0);
        fr.drawString(text, x, y, color);
    }

    public static void renderSmokeNadeSmoke(ResourceLocation par1, double par2, double par3, double par4, float par5, int width, int height, String color, double alpha) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        FontRenderer fontrenderer = mc.fontRenderer;
        float f122 = 1.8f;
        float scale2 = 0.02f;
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glScalef((float) (-scale2), (float) (-scale2), (float) scale2);
        GL11.glDepthMask((boolean) false);
        GL11.glEnable((int) 3042);
        GuiUtils.renderColor(color);
        float realTick = 0;
        for (int i1 = 0; i1 < 4; ++i1) {
            float val = (float) Math.sin(realTick / 100.0f);
            if (i1 % 2 == 0) {
                val = -val;
            }
            for (int i = 0; i < 9; ++i) {
                GuiUtils.drawImage((-width) / 2, (-height) / 2, par1, width, height);
                GL11.glRotated((double) 64.0, (double) 0.0, (double) 1.0, (double) 0.0);
                GL11.glRotated((double) val, (double) 1.0, (double) 0.0, (double) 0.0);
            }
            GL11.glRotated((double) 90.0, (double) 1.0, (double) 0.0, (double) 0.0);
        }
        GL11.glDepthMask((boolean) true);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void renderCenteredTextScaled(String text, int posX, int posY, double par4, int givenColor) {
        Minecraft mc = Minecraft.getMinecraft();
        double width = (double) (mc.fontRenderer.getStringWidth(text) / 2) * par4;
        GL11.glPushMatrix();
        GL11.glTranslated((double) ((double) posX - width), (double) posY, (double) 0.0);
        GL11.glScaled((double) par4, (double) par4, (double) par4);
        mc.fontRenderer.drawString(text, 0, 0, givenColor);
        GL11.glPopMatrix();
    }

    public static void renderCenteredTextScaledWithOutlineFade(String text, int posX, int posY, double par4, int givenColor, int givenFade) {
        Minecraft mc = Minecraft.getMinecraft();
        double width = (double) (mc.fontRenderer.getStringWidth(text) / 2) * par4;
        GL11.glPushMatrix();
        GL11.glColor4f((float) 255.0f, (float) 255.0f, (float) 255.0f, (float) givenFade);
        GL11.glTranslated((double) ((double) posX - width), (double) posY, (double) 0.0);
        GL11.glScaled((double) par4, (double) par4, (double) par4);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, -1, -1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 1, -1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, -1, 1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 1, 1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 0, -1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, -1, 0, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 1, 0, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 0, 1, 0);
        mc.fontRenderer.drawString(text, 0, 0, givenColor);
        GL11.glPopMatrix();
    }

    public static void renderCenteredTextScaledWithOutline(String text, int posX, int posY, double par4, int givenColor) {
        Minecraft mc = Minecraft.getMinecraft();
        double width = (double) (mc.fontRenderer.getStringWidth(text) / 2) * par4;
        GL11.glPushMatrix();
        GL11.glTranslated((double) ((double) posX - width), (double) posY, (double) 0.0);
        GL11.glScaled((double) par4, (double) par4, (double) par4);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, -1, -1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 1, -1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, -1, 1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 1, 1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 0, -1, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, -1, 0, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 1, 0, 0);
        mc.fontRenderer.drawString((Object) EnumChatFormatting.BLACK + text, 0, 1, 0);
        mc.fontRenderer.drawString(text, 0, 0, givenColor);
        GL11.glPopMatrix();
    }

    public static void drawTextScaled(String text, int posX, int posY, double par4, int par5) {
        GL11.glPushMatrix();
        GL11.glTranslated((double) posX, (double) posY, (double) 0.0);
        GL11.glScaled((double) par4, (double) par4, (double) par4);
        GuiUtils.renderText(text, 0, 0, par5);
        GL11.glPopMatrix();
    }

    public static void renderTextWithShadow(String text, int posX, int posY, int color) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.fontRenderer.drawStringWithShadow(text, posX, posY, color);
    }

    public static void renderSplitText(String text, int posX, int posY, int wrapWidth, int color) {
        Minecraft mc = Minecraft.getMinecraft();
        mc.fontRenderer.drawSplitString(text, posX, posY, wrapWidth, color);
    }

    public static void renderItemStackIntoGUI(ItemStack itemstack, int posX, int posY) {
        GL11.glPushMatrix();
        RenderItem itemRenderer = new RenderItem();
        itemRenderer.renderWithColor = true;
        itemRenderer.renderItemIntoGUI(Minecraft.getMinecraft().fontRenderer, Minecraft.getMinecraft().getTextureManager(), itemstack, posX, posY);
        itemRenderer.renderWithColor = false;
        GL11.glDisable((int) 2896);
        GL11.glPopMatrix();
    }

    public static void renderColor(int par1) {
        Color color = Color.decode("" + par1);
        float red = (float) color.getRed() / 255.0f;
        float green = (float) color.getGreen() / 255.0f;
        float blue = (float) color.getBlue() / 255.0f;
        GL11.glColor3f((float) red, (float) green, (float) blue);
    }

    public static void renderColor(String par1) {
        Color color = Color.decode("" + par1);
        float red = (float) color.getRed() / 255.0f;
        float green = (float) color.getGreen() / 255.0f;
        float blue = (float) color.getBlue() / 255.0f;
        GL11.glColor3f((float) red, (float) green, (float) blue);
    }

    public static void drawImageNoColorChange(double x, double y, ResourceLocation image, double width, double height) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        GL11.glHint((int) 3153, (int) 4353);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 3042);
        GL11.glDisable((int) 2832);
    }

    public static void drawImageCenteredScaled(double x, double y, ResourceLocation image, double width, double height, double scale) {
        GL11.glColor3f((float) 1.0f, (float) 1.0f, (float) 1.0f);
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        GL11.glTranslated((double) ((-width) / 2.0 * scale * scale), (double) ((-height) / 2.0 * scale * scale), (double) 0.0);
        GL11.glScaled((double) scale, (double) scale, (double) scale);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        GL11.glHint((int) 3153, (int) 4353);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 3042);
        GL11.glDisable((int) 2832);
    }

    public static void drawImage(double x, double y, ResourceLocation image, double width, double height) {
        GL11.glColor3f((float) 1.0f, (float) 1.0f, (float) 1.0f);
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        GL11.glHint((int) 3153, (int) 4353);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 3042);
        GL11.glDisable((int) 2832);
    }

    public static void drawImageTransparent(double x, double y, ResourceLocation image, double width, double height) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        GL11.glColor4d((double) 255.0, (double) 255.0, (double) 255.0, (double) 255.0);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        tess.setColorRGBA_F(255.0f, 255.0f, 255.0f, 255.0f);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 2832);
        GL11.glDisable((int) 3042);
    }

    public static void drawImageTransparent(double x, double y, ResourceLocation image, double width, double height, double alpha) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        GL11.glColor4d((double) 255.0, (double) 255.0, (double) 255.0, (double) alpha);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        tess.setColorRGBA_F(255.0f, 0.0f, 0.0f, 0.5f);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 2832);
        GL11.glDisable((int) 3042);
    }

    public static void drawMenuBackground(double x, double y, ResourceLocation image, double width, double height) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        tess.setColorRGBA_F(255.0f, 0.0f, 0.0f, 0.5f);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 2832);
        GL11.glDisable((int) 3042);
    }


    public static void drawPositionedImageInViewNoRot(ResourceLocation par1, double par2, double par3, double par4, float par5, int width, int height, double givenRotX, double givenRotY, double givenRotZ) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        GL11.glDepthMask((boolean) false);
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glRotated((double) givenRotX, (double) 1.0, (double) 0.0, (double) 0.0);
        GL11.glRotated((double) givenRotY, (double) 0.0, (double) 1.0, (double) 0.0);
        GL11.glRotated((double) givenRotZ, (double) 0.0, (double) 0.0, (double) 1.0);
        GuiUtils.renderColor(16777215);
        GuiUtils.drawImage((-width) / 2, (-height) / 2, par1, width, height);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 2929);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void drawFlare(ResourceLocation par1, double par2, double par3, double par4, float par5, float width, float height, int givenR, int givenG, int givenB) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) (-player.rotationYaw), (float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) player.rotationPitch, (float) 1.0f, (float) 0.0f, (float) 0.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        OpenGlHelper.setLightmapTextureCoords((int) OpenGlHelper.lightmapTexUnit, (float) 240.0f, (float) 240.0f);
        GL11.glColor3f((float) givenR, (float) givenG, (float) givenB);
        GuiUtils.drawImageNoColorChange((-width) / 2.0f, (-height) / 2.0f, par1, width, height);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void drawPositionedImageInView(ResourceLocation par1, double par2, double par3, double par4, float par5, float width, float height) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) (-player.rotationYaw), (float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) player.rotationPitch, (float) 1.0f, (float) 0.0f, (float) 0.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        if (dM) {
            GL11.glDepthMask((boolean) false);
        }
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GuiUtils.renderColor(16777215);
        GuiUtils.drawImage((-width) / 2.0f, (-height) / 2.0f, par1, width, height);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 2929);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void drawPositionedImageInViewWithDepth(ResourceLocation par1, double par2, double par3, double par4, float par5, float width, float height, float givenAlpha) {
        EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) (-player.rotationYaw), (float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) player.rotationPitch, (float) 1.0f, (float) 0.0f, (float) 0.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glEnable((int) 3008);
        GuiUtils.drawImageTransparent((-width) / 2.0f, (-height) / 2.0f, par1, width, height, givenAlpha);
        GL11.glEnable((int) 2896);
        GL11.glPopMatrix();
    }

    public static void drawPositionedTextInView(String par1, double par2, double par3, double par4, float par5) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glRotatef((float) (-player.rotationYaw), (float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) player.rotationPitch, (float) 1.0f, (float) 0.0f, (float) 0.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        if (dM) {
            GL11.glDepthMask((boolean) false);
        }
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GuiUtils.renderColor(16777215);
        GuiUtils.drawTextWithOutline(par1, 0, 0, 16777215);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 2929);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void drawPositionedTextInViewScaled(String par1, double par2, double par3, double par4, float par5, double scale, int givenColor) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glRotatef((float) (-player.rotationYaw), (float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) player.rotationPitch, (float) 1.0f, (float) 0.0f, (float) 0.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        if (dM) {
            GL11.glDepthMask((boolean) false);
        }
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GuiUtils.renderColor(16777215);
        GuiUtils.renderCenteredTextScaledWithOutline(par1, 0, 0, scale, givenColor);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 2929);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void drawPositionedTextInViewScaled(String par1, double par2, double par3, double par4, float par5, double scale) {
        Minecraft mc = Minecraft.getMinecraft();
        EntityClientPlayerMP player = mc.thePlayer;
        GL11.glPushMatrix();
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) par5;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) par5;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) par5;
        GL11.glTranslated((double) par2, (double) par3, (double) par4);
        GL11.glTranslated((double) (-d0), (double) (-d1), (double) (-d2));
        GL11.glNormal3f((float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glRotatef((float) (-player.rotationYaw), (float) 0.0f, (float) 1.0f, (float) 0.0f);
        GL11.glRotatef((float) player.rotationPitch, (float) 1.0f, (float) 0.0f, (float) 0.0f);
        GL11.glScalef((float) -0.03f, (float) -0.03f, (float) 0.03f);
        GL11.glDisable((int) 2896);
        if (dM) {
            GL11.glDepthMask((boolean) false);
        }
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GuiUtils.renderColor(16777215);
        GuiUtils.renderCenteredTextScaledWithOutline(par1, 0, 0, scale, 16777215);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 2929);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    public static void renderBeam(World world, double p_147500_2_, double p_147500_4_, double p_147500_6_, float p_147500_8_) {
        GL11.glAlphaFunc((int) 516, (float) 0.1f);
        Tessellator tessellator = Tessellator.instance;
        Minecraft.getMinecraft().renderEngine.bindTexture(beaconBeam);
        GL11.glTexParameterf((int) 3553, (int) 10242, (float) 10497.0f);
        GL11.glTexParameterf((int) 3553, (int) 10243, (float) 10497.0f);
        GL11.glDisable((int) 2896);
        GL11.glDisable((int) 2884);
        GL11.glDisable((int) 3042);
        GL11.glDepthMask((boolean) true);
        OpenGlHelper.glBlendFunc((int) 770, (int) 1, (int) 1, (int) 0);
        float f2 = (float) world.getTotalWorldTime() + p_147500_8_;
        float f3 = (-f2) * 0.2f - (float) MathHelper.floor_float((float) ((-f2) * 0.1f));
        boolean b0 = true;
        double d3 = 0.025;
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 32);
        double d5 = 0.2;
        double d7 = 0.5 + Math.cos(d3 + 2.356194490192345) * d5;
        double d9 = 0.5 + Math.sin(d3 + 2.356194490192345) * d5;
        double d11 = 0.5 + Math.cos(d3 + 0.7853981633974483) * d5;
        double d13 = 0.5 + Math.sin(d3 + 0.7853981633974483) * d5;
        double d15 = 0.5 + Math.cos(d3 + 3.9269908169872414) * d5;
        double d17 = 0.5 + Math.sin(d3 + 3.9269908169872414) * d5;
        double d19 = 0.5 + Math.cos(d3 + 5.497787143782138) * d5;
        double d21 = 0.5 + Math.sin(d3 + 5.497787143782138) * d5;
        double d23 = 256.0;
        double d25 = 0.0;
        double d27 = 1.0;
        double d28 = -1.0f + f3;
        double d29 = 256.0 * (0.5 / d5) + d28;
        tessellator.addVertexWithUV(p_147500_2_ + d7, p_147500_4_ + d23, p_147500_6_ + d9, d27, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d7, p_147500_4_, p_147500_6_ + d9, d27, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d11, p_147500_4_, p_147500_6_ + d13, d25, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d11, p_147500_4_ + d23, p_147500_6_ + d13, d25, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d19, p_147500_4_ + d23, p_147500_6_ + d21, d27, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d19, p_147500_4_, p_147500_6_ + d21, d27, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d15, p_147500_4_, p_147500_6_ + d17, d25, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d15, p_147500_4_ + d23, p_147500_6_ + d17, d25, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d11, p_147500_4_ + d23, p_147500_6_ + d13, d27, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d11, p_147500_4_, p_147500_6_ + d13, d27, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d19, p_147500_4_, p_147500_6_ + d21, d25, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d19, p_147500_4_ + d23, p_147500_6_ + d21, d25, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d15, p_147500_4_ + d23, p_147500_6_ + d17, d27, d29);
        tessellator.addVertexWithUV(p_147500_2_ + d15, p_147500_4_, p_147500_6_ + d17, d27, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d7, p_147500_4_, p_147500_6_ + d9, d25, d28);
        tessellator.addVertexWithUV(p_147500_2_ + d7, p_147500_4_ + d23, p_147500_6_ + d9, d25, d29);
        tessellator.draw();
        GL11.glEnable((int) 3042);
        OpenGlHelper.glBlendFunc((int) 770, (int) 771, (int) 1, (int) 0);
        GL11.glDepthMask((boolean) false);
        tessellator.startDrawingQuads();
        tessellator.setColorRGBA(255, 255, 255, 32);
        double d30 = 0.2;
        double d4 = 0.2;
        double d6 = 0.8;
        double d8 = 0.2;
        double d10 = 0.2;
        double d12 = 0.8;
        double d14 = 0.8;
        double d16 = 0.8;
        double d18 = 256.0;
        double d20 = 0.0;
        double d22 = 1.0;
        double d24 = -1.0f + f3;
        double d26 = 256.0 + d24;
        tessellator.addVertexWithUV(p_147500_2_ + d30, p_147500_4_ + d18, p_147500_6_ + d4, d22, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d30, p_147500_4_, p_147500_6_ + d4, d22, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d6, p_147500_4_, p_147500_6_ + d8, d20, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d6, p_147500_4_ + d18, p_147500_6_ + d8, d20, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d14, p_147500_4_ + d18, p_147500_6_ + d16, d22, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d14, p_147500_4_, p_147500_6_ + d16, d22, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d10, p_147500_4_, p_147500_6_ + d12, d20, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d10, p_147500_4_ + d18, p_147500_6_ + d12, d20, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d6, p_147500_4_ + d18, p_147500_6_ + d8, d22, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d6, p_147500_4_, p_147500_6_ + d8, d22, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d14, p_147500_4_, p_147500_6_ + d16, d20, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d14, p_147500_4_ + d18, p_147500_6_ + d16, d20, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d10, p_147500_4_ + d18, p_147500_6_ + d12, d22, d26);
        tessellator.addVertexWithUV(p_147500_2_ + d10, p_147500_4_, p_147500_6_ + d12, d22, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d30, p_147500_4_, p_147500_6_ + d4, d20, d24);
        tessellator.addVertexWithUV(p_147500_2_ + d30, p_147500_4_ + d18, p_147500_6_ + d4, d20, d26);
        tessellator.draw();
        GL11.glEnable((int) 2896);
        GL11.glEnable((int) 3553);
        GL11.glDepthMask((boolean) true);
    }


    public static void drawRect(int x, int y, int width, int height, String colorString, float alpha) {
        Color color = Color.decode(colorString);
        float red = (float) color.getRed() / 255.0f;
        float green = (float) color.getGreen() / 255.0f;
        float blue = (float) color.getBlue() / 255.0f;
        GuiUtils.drawRect(x, y, x + width, y + height, red, green, blue, alpha);
    }

    public static void drawRect(int xStart, int yStart, int xEnd, int yEnd, int rgb) {
        Tessellator tessellator = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glDisable((int) 3553);
        GL11.glColor3b((byte) ((byte) (rgb >> 16)), (byte) ((byte) (rgb >> 8 & 255)), (byte) ((byte) (rgb & 255)));
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) xStart, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yStart, 0.0);
        tessellator.addVertex((double) xStart, (double) yStart, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glPopMatrix();
    }

    public static void drawRect(int xStart, int yStart, int xEnd, int yEnd, float r, float g, float b, float alpha) {
        Tessellator tessellator = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glDisable((int) 3553);
        GL11.glColor4f((float) r, (float) g, (float) b, (float) alpha);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glDisable((int) 3553);
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) xStart, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yStart, 0.0);
        tessellator.addVertex((double) xStart, (double) yStart, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
        GL11.glDisable((int) 3042);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glPopMatrix();
    }

    public static void drawRect2(int xStart, int yStart, int xEnd, int yEnd, Color color) {
        float r = (float) color.getRed() / 255.0f;
        float g = (float) color.getGreen() / 255.0f;
        float b = (float) color.getBlue() / 255.0f;
        float a = (float) color.getAlpha() / 255.0f;
        Tessellator tessellator = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glDisable((int) 3553);
        GL11.glColor4f((float) r, (float) g, (float) b, (float) a);
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) xStart, (double) (yStart + yEnd), 0.0);
        tessellator.addVertex((double) (xStart + xEnd), (double) (yStart + yEnd), 0.0);
        tessellator.addVertex((double) (xStart + xEnd), (double) yStart, 0.0);
        tessellator.addVertex((double) xStart, (double) yStart, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
        GL11.glDisable((int) 3042);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glPopMatrix();
    }

    public static void drawRectWithShadow2(int x, int y, int width, int height, Color color, int alpha) {
        GuiUtils.drawRect2(x - 1, y - 1, width + 2, height + 2, new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha / 2));
        GuiUtils.drawRect2(x, y, width, height, new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha));
    }

    public static void drawRectWithShadow(int par0, int par1, int par2, int par3, String par4Hex, float par5Alpha) {
        GuiUtils.drawRect(par0 - 1, par1 - 1, par2 + 2, par3 + 2, "0x000000", 0.2f);
        GuiUtils.drawRect(par0, par1, par2, par3, par4Hex, par5Alpha);
    }

    public static void drawRectWithShadow(int par0, int par1, int par2, int par3, String par4Hex, float par5Alpha, float par6Alpha) {
        GuiUtils.drawRect(par0 - 1, par1 - 1, par2 + 2, par3 + 2, "0x000000", par6Alpha);
        GuiUtils.drawRect(par0, par1, par2, par3, par4Hex, par5Alpha);
    }

    public static boolean isInBox(int x, int y, int width, int height, int checkX, int checkY) {
        return checkX >= x && checkY >= y && checkX <= x + width && checkY <= y + height;
    }

    public static void drawTexturedQuadFit(int width, int height) {
        Tessellator t = Tessellator.instance;
        t.startDrawingQuads();
        t.addVertexWithUV(10.0, (double) (10 + height), 1.0, 0.0, 1.0);
        t.addVertexWithUV((double) (10 + width), (double) (10 + height), 1.0, 1.0, 1.0);
        t.addVertexWithUV((double) (10 + width), 10.0, 1.0, 1.0, 0.0);
        t.addVertexWithUV(10.0, 10.0, 1.0, 0.0, 0.0);
        t.draw();
    }

    public static void drawOutlinedBoundingBox(AxisAlignedBB par1AxisAlignedBB) {
        Tessellator var2 = Tessellator.instance;
        var2.startDrawing(3);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
        var2.draw();
        var2.startDrawing(3);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
        var2.draw();
        var2.startDrawing(1);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
        var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
        var2.draw();
    }

    public static void drawSelectionBox(EntityPlayer player, float particleTicks, AxisAlignedBB boundingBox, Color givenColor) {
        GL11.glPushMatrix();
        GL11.glDisable((int) 3008);
        GL11.glDisable((int) 2896);
        GL11.glDepthMask((boolean) false);
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        float colorR = givenColor.getRed();
        float colorG = givenColor.getGreen();
        float colorB = givenColor.getBlue();
        GL11.glColor4f((float) colorR, (float) colorG, (float) colorB, (float) 0.7f);
        GL11.glLineWidth((float) 2.0f);
        GL11.glDisable((int) 3553);
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) particleTicks;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) particleTicks;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) particleTicks;
        GuiUtils.drawOutlinedBoundingBox(boundingBox.getOffsetBoundingBox(-d0, -d1, -d2));
        GL11.glEnable((int) 2929);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 3553);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glEnable((int) 3008);
        GL11.glColor4f((float) 0.0f, (float) 0.0f, (float) 0.0f, (float) 255.0f);
        GL11.glPopMatrix();
    }

    public static void drawLine(int posX1, int posY1, int posZ1, int posX2, int posY2, int posZ2, EntityPlayer player, float particleTicks, Color givenColor) {
        GL11.glPushMatrix();
        GL11.glDisable((int) 3008);
        GL11.glDisable((int) 2896);
        GL11.glDepthMask((boolean) false);
        GL11.glDisable((int) 2929);
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        float colorR = givenColor.getRed();
        float colorG = givenColor.getGreen();
        float colorB = givenColor.getBlue();
        GL11.glColor4f((float) colorR, (float) colorG, (float) colorB, (float) 0.7f);
        GL11.glLineWidth((float) 2.0f);
        GL11.glDisable((int) 3553);
        double d0 = player.lastTickPosX + (player.posX - player.lastTickPosX) * (double) particleTicks;
        double d1 = player.lastTickPosY + (player.posY - player.lastTickPosY) * (double) particleTicks;
        double d2 = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * (double) particleTicks;
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) posX1, (double) posY1, (double) posZ1);
        tessellator.addVertex((double) (posX1 + 2), (double) (posY1 + 2), (double) (posZ1 + 2));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) posX2, (double) posY2, (double) posZ2);
        tessellator.addVertex((double) posX2, (double) posY2, (double) posZ2);
        tessellator.draw();
        GL11.glEnable((int) 2929);
        GL11.glDepthMask((boolean) true);
        GL11.glEnable((int) 3553);
        GL11.glEnable((int) 2896);
        GL11.glDisable((int) 3042);
        GL11.glEnable((int) 3008);
        GL11.glColor4f((float) 0.0f, (float) 0.0f, (float) 0.0f, (float) 255.0f);
        GL11.glPopMatrix();
    }


    public static ResourceLocation getLocationSkin(String username) {
        return new ResourceLocation("skins/" + StringUtils.stripControlCodes((String) username));
    }

    public static String secondsToTimeFormatted(long sec) {
        long seconds = sec % 60L;
        long minutes = sec / 60L;
        if (minutes >= 60L) {
            long hours = minutes / 60L;
            minutes %= 60L;
            if (hours >= 24L) {
                long days = hours / 24L;
                return String.format("%d days %02d:%02d:%02d", days, hours % 24L, minutes, seconds);
            }
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }
        return String.format("00:%02d:%02d", minutes, seconds);
    }

    public static void renderAABB(AxisAlignedBB p_76980_0_) {
        Tessellator tessellator = Tessellator.instance;
        tessellator.startDrawingQuads();
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.maxY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.maxY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.minY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.minY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.minY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.minY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.maxY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.maxY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.minY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.maxY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.maxY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.minX, p_76980_0_.minY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.minY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.maxY, p_76980_0_.minZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.maxY, p_76980_0_.maxZ);
        tessellator.addVertex(p_76980_0_.maxX, p_76980_0_.minY, p_76980_0_.maxZ);
        tessellator.draw();
    }

    public static void renderOffsetAABB(AxisAlignedBB p_76978_0_, double p_76978_1_, double p_76978_3_, double p_76978_5_) {
        GL11.glDisable((int) 3553);
        Tessellator tessellator = Tessellator.instance;
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        tessellator.startDrawingQuads();
        tessellator.setTranslation(p_76978_1_, p_76978_3_, p_76978_5_);
        tessellator.setNormal(0.0f, 0.0f, -1.0f);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.maxY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.maxY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.minY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.minY, p_76978_0_.minZ);
        tessellator.setNormal(0.0f, 0.0f, 1.0f);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.minY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.minY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.maxY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.maxY, p_76978_0_.maxZ);
        tessellator.setNormal(0.0f, -1.0f, 0.0f);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.minY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.minY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.minY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.minY, p_76978_0_.maxZ);
        tessellator.setNormal(0.0f, 1.0f, 0.0f);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.maxY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.maxY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.maxY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.maxY, p_76978_0_.minZ);
        tessellator.setNormal(-1.0f, 0.0f, 0.0f);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.minY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.maxY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.maxY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.minX, p_76978_0_.minY, p_76978_0_.minZ);
        tessellator.setNormal(1.0f, 0.0f, 0.0f);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.minY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.maxY, p_76978_0_.minZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.maxY, p_76978_0_.maxZ);
        tessellator.addVertex(p_76978_0_.maxX, p_76978_0_.minY, p_76978_0_.maxZ);
        tessellator.setTranslation(0.0, 0.0, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
    }

    public void drawBoundingBox(AxisAlignedBB boundingBox) {
        Tessellator tessellator = Tessellator.instance;
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.draw();
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        tessellator.draw();
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        tessellator.draw();
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        tessellator.draw();
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        tessellator.draw();
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
        tessellator.addVertex(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
        tessellator.draw();
    }
}