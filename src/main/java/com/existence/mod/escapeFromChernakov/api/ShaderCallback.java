package com.existence.mod.escapeFromChernakov.api;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 10.05.2018
 * Time: 20:46
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public abstract class ShaderCallback {

    public abstract void call(int shader);
}
