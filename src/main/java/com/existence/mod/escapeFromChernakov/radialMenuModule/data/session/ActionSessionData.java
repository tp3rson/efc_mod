package com.existence.mod.escapeFromChernakov.radialMenuModule.data.session;


import com.existence.mod.escapeFromChernakov.radialMenuModule.data.click.ClickAction;
import com.existence.mod.escapeFromChernakov.radialMenuModule.handler.LogHandler;

import java.util.EnumSet;

/**
 * @author tp3rson
 */
public class ActionSessionData {

    public static EnumSet<ClickAction> availableActions = EnumSet.noneOf(ClickAction.class);

    public static void activateAll() {
        availableActions.clear();
        availableActions.addAll(ClickAction.getValues());
    }

    public static void activateClientValues() {
        availableActions.clear();
        availableActions.addAll(ClickAction.getClientValues());
    }

    public static void restoreDefaults() {
        LogHandler.info("CLIENT: Disconnected from server, restoring default security settings");
        activateAll();
    }
}
