package com.existence.mod.escapeFromChernakov.mainModule.backend.client.gui.mainMenu;

import com.existence.mod.escapeFromChernakov.mainModule.EFCMain;
import com.existence.mod.escapeFromChernakov.util.EFCUtils;
import com.google.common.collect.ImmutableList;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.util.List;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 17.06.2018
 * Time: 12:45
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class GuiMainMenuHome extends EFCParalaxMainMenu {

    int mouseX;
    int mouseY;
    private static final String GETTING_LIST_TEXT = (Object) EnumChatFormatting.DARK_RED + "Searching Servers..." + (Object) EnumChatFormatting.RESET;
    private static final String PINGING_TEXT = (Object) EnumChatFormatting.DARK_RED + "Pinging Servers..." + (Object) EnumChatFormatting.RESET;
    public float fade;
    //private final GuiScroller newsContainer;
    static PingState pingState;
    private boolean init;
    private int imageWidth = 247;
    private int imageHeight = 50;

    public GuiMainMenuHome() {
        this.mouseX = Minecraft.getMinecraft().mouseHelper.deltaX;
        this.mouseY = Minecraft.getMinecraft().mouseHelper.deltaY;
        this.fade = 2.0f;
        //this.newsContainer = new GuiScroller(3, this.height, 239, 157, this);
        this.init = false;
        this.imageWidth = 247;
        this.imageHeight = 50;
    }

    @Override
    public void initGui() {
        Minecraft mc = Minecraft.getMinecraft();
        int x = this.width / 2 - 173;
        super.initGui();
        int y = this.height - 22;
        this.initOverheadMenu();
    }

    private void initPing() {
        PingState state;
        pingState = state = new PingState();
        state.pingText = GETTING_LIST_TEXT;
        //ServersManager.getServerList().whenCompleteAsync((servers, x) -> {
        //            this.handlePlayerCountList(state, servers, x);
        //        }
        //);
    }

    private void initNews() {
        //this.newsContainer.setTextList(Collections.singletonList("&cLoading News..."));
        //NewsManager.fetchNews().whenCompleteAsync((arg_0, arg_1) -> this.injectNews(arg_0, arg_1), (Executor)((Object)Scheduler.client()));
    }

    private void injectNews(List<String> news, Throwable x) {
        if (news != null) {
            //this.newsContainer.setTextList(news);
        } else {
            //this.newsContainer.setTextList((List<String>)ImmutableList.of((Object)"Failed to grab news"));
        }
    }

    //private void handlePlayerCountList(PingState state, List<Server> list, Throwable err) {
    //    if (pingState == state) {
    //        if (err == null) {
    //            state.pingText = PINGING_TEXT;
    //            state.servers = list;
    //            state.playerCount = -1;
    //            state.serverTexts = list.stream().map(s -> GuiMainMenuHome.getServerStatusText(s, "Pinging...")).collect(Collectors.toCollection(ArrayList::new));
    //            int i = 0;
    //            while (i < list.size()) {
    //                Server server = list.get(i);
    //                int thisI = i++;
    //                server.getPingData(true).whenCompleteAsync((reply, x) -> {
    //                            GuiMainMenuHome.handlePingComplete(state, thisI, reply, x);
    //                        }
    //                        , (Executor)((Object)Scheduler.client()));
    //            }
    //        } else {
    //            err.printStackTrace();
    //            state.pingText = "Couldn't get server list";
    //        }
    //    }
    //}

    //private static void handlePingComplete(PingState state, int serverIndex, MinecraftPingReply reply, Throwable x) {
    //    if (pingState == state) {
    //        ++state.completedPings;
    //        Server server = state.servers.get(serverIndex);
    //        if (x == null) {
    //            int onlinePlayers = reply.getPlayers().getOnline();
    //            state.playerCount = Math.max(0, state.playerCount) + onlinePlayers;
    //            state.pingText = (Object)EnumChatFormatting.WHITE + "Survivors Online: " + state.playerCount + (Object)EnumChatFormatting.RESET;
    //            state.serverTexts.set(serverIndex, GuiMainMenuHome.getServerStatusText(server, String.format("%s / %s", onlinePlayers, reply.getPlayers().getMax())));
    //        } else {
    //            state.serverTexts.set(serverIndex, GuiMainMenuHome.getServerStatusText(server, "Not reachable"));
    //        }
    //        if (state.completedPings == state.servers.size() && state.playerCount == -1) {
    //            state.pingText = "Not reachable";
    //        }
    //    }
    //}

    //private static String getServerStatusText(Server server, String status) {
    //    return server.getName() + ": " + status;
    //}

    @Override
    public void actionPerformed(GuiButton button) {
        if (this.fade <= 0.1f) {
            super.actionPerformed(button);
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        super.drawScreen(mouseX, mouseY, partialTicks);
        if (EFCMain.efcInstance.isMainMenuLoaded) {
            this.fade = 0.0f;
            return;
        }
        this.fade -= 0.01f;
        if (this.fade <= 0.0f) {
            EFCMain.efcInstance.isMainMenuLoaded = true;
        }
        this.drawIntroLogo();
    }

    public void drawIntroLogo() {
        EFCUtils.drawRectTransparent(0, 0, this.width, this.height, "0x000000", this.fade);
        GL11.glPushMatrix();
        GL11.glEnable((int) 3042);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) this.fade);
        ScaledResolution sR = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
        EFCUtils.drawImage(sR.getScaledWidth() / 2 - 123, sR.getScaledHeight() / 2 - 25, new ResourceLocation("escapefromchernakov:textures/gui/graphic_main.png"), 247.0, 50.0);
        GL11.glDisable((int) 3042);
        GL11.glPopMatrix();
    }

    @Override
    public void initOverheadMenu() {
        int x = this.width / 2 - 173;
        int y = this.height - 29;
        int buttonWidth = 85;
        //UnicodeFontRenderer.renderString("Play", x + 1, y);
        this.buttonList.add(new GuiCustomPressable(2, x + 1, y, buttonWidth + 31, 28, "Играть"));
        this.buttonList.add(new GuiCustomPressable(3, x + 33 + buttonWidth, y, buttonWidth, 28, "Поддержать"));
        this.buttonList.add(new GuiCustomPressable(4, x + 119 + buttonWidth, y, buttonWidth, 28, "Инфо"));
        this.buttonList.add(new GuiCustomPressable(5, x + 35 + 3 * buttonWidth, y, 30, 28, this.iconSettings));
        this.buttonList.add(new GuiCustomPressable(6, x + 66 + 3 * buttonWidth, y, 30, 28, this.iconPower));
        this.buttonList.add(new GuiCustomPressable(7, 0, 50, 60, 18, "News     >>"));
        this.buttonList.add(new GuiCustomPressable(8, 0, 69, 60, 18, "Updates >>"));
        this.buttonList.add(new GuiCustomPressable(0, 0, 88, 60, 18, "Soon" + " >>"));
    }

    static class PingState {
        int playerCount;
        String pingText;
        //List<Server> servers;
        int completedPings;
        List<String> serverTexts = ImmutableList.of();

        PingState() {
        }
    }
}