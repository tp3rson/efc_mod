package com.existence.mod.escapeFromChernakov.mainModule.backend.client.gui.mainMenu;

import com.existence.mod.escapeFromChernakov.util.EFCUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 17.06.2018
 * Time: 12:39
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class EFCParalaxMainMenu extends net.minecraft.client.gui.GuiScreen {
    int x, y = 0;

    public static int backgroundX = 0;
    public static int backgroundY = 0;
    public static int backgroundFog1X = 0;
    public static int backgroundFog1Y = 0;
    public static int backgroundFog2X = 0;
    public static int backgroundFog2Y = 0;

    private java.util.List<GuiParticleMiscEffect> fireFlies = new ArrayList<>();

    public static ResourceLocation background;
    public static ResourceLocation backgroundFog;

    static {
        background = new ResourceLocation("escapefromchernakov:textures/gui/background.png");
        backgroundFog = new ResourceLocation("escapefromchernakov:textures/gui/background_fog.png");
    }

    public ResourceLocation iconHome = new ResourceLocation("escapefromchernakov:textures/gui/home.png");
    public ResourceLocation iconSettings = new ResourceLocation("escapefromchernakov:textures/gui/settings.png");
    public ResourceLocation iconPower = new ResourceLocation("escapefromchernakov:textures/gui/power.png");
    private static final URI DONATE;
    //private static final URI DISCORD;
    //private static final URI NEWSIMAGE_1;
    //private static final URI NEWSIMAGE_2;
    //private static final URI UPDATE;

    @Override
    public void initGui() {
        super.initGui();
        int x = this.width / 2 - 173;
        this.initOverheadMenu();
    }

    @Override
    public void actionPerformed(GuiButton button) {
        super.actionPerformed(button);
        this.addOverheadMenuActionPerformed(button);
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {

        drawGradientRect(0, 0, this.width, this.height, 0xFF000000, 0xFF000000);

        this.drawBackground(mouseX, mouseY, partialTicks);
        EFCUtils.drawRectWithShadow(0, 0, this.width, 30, Color.black, 50);
        EFCUtils.drawRectWithShadow(0, this.height - 30, this.width, 30, Color.black, 50);

        Random rand = new Random();
        if (rand.nextInt(16) == 0) {
            fireFlies.add(new GuiParticleMiscEffect(this.width + 50, rand.nextInt(this.height), rand.nextFloat() - rand.nextFloat(), rand.nextFloat() - rand.nextFloat()));
        }

        for (int i = 0; i < fireFlies.size(); i++) {
            if (fireFlies.get(i).posY < this.height + 40) {
                fireFlies.get(i).drawFireFly(this.mc);
            } else {
                fireFlies.remove(i);
            }
        }

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    public void addOverheadMenuActionPerformed(GuiButton button) {
        switch (button.id) {
            case 1: {
                if (!(this instanceof GuiMainMenuHome)) {
                    Minecraft.getMinecraft().displayGuiScreen(new GuiMainMenuHome());
                    break;
                }
                (this).initGui();
                break;
            }
            case 2: {
                //if(Backend.server)
                Minecraft.getMinecraft().displayGuiScreen(new GuiSelectWorld(this));
                //Minecraft.getMinecraft().displayGuiScreen(new GuiMultiplayer(this));

                //if (Backend.client)
                //Minecraft.getMinecraft().displayGuiScreen(new GuiMultiplayer(this));
                //ServersManager.getServerList().whenCompleteAsync((list, x) -> {
                //            if (x != null) {
                //                //Mod.log.error("Failed to get servers", x);
                //            } else {
                //                Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new GuiMainMenuServersList(list, this instanceof GuiMainMenuServersList));
                //            }
                //        });
                //, (Executor)((Object)Scheduler.client()));
                break;
            }
            case 3: {
                EFCParalaxMainMenu.openURL(DONATE);
                break;
            }
            case 4: {
                Minecraft.getMinecraft().displayGuiScreen(new GuiMultiplayer(this));
                break;
            }
            case 5: {
                Minecraft.getMinecraft().displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;
            }
            case 6: {
                Minecraft.getMinecraft().shutdown();
                break;
            }
            case 7: {
                Minecraft.getMinecraft().displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;
            }
            case 8: {
                Minecraft.getMinecraft().displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;
            }
            case 9: {
                Minecraft.getMinecraft().displayGuiScreen(new GuiMultiplayer(this));
                break;
            }
            case 10: {
                Minecraft.getMinecraft().displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
            }
        }
    }

    private static void openURL(URI uri) {
        try {
            Desktop.getDesktop().browse(uri);
        } catch (IOException e) {
            //log.warn("Failed to open link");
        }
    }

    public void drawBackground(int mouseX, int mouseY, float partialTicks) {
        int backgroundLength = (-this.width) * 2;
        if ((this.backgroundFog1X -= 2) <= backgroundLength) {
            this.backgroundFog1X = 1;
        }
        if ((this.backgroundFog2X += 2) >= this.width * 2) {
            this.backgroundFog2X = 1;
        }
        EFCUtils.drawImage(0.0, 0.0, this.background, this.width, this.height);
        GL11.glPushMatrix();
        GL11.glScaled(0.5, 0.5, 0.5);
        GL11.glColor4f(1.0f, 1.0f, 1.0f, 0.4f);
        GL11.glEnable(3042);
        EFCUtils.drawImageTransparent(this.backgroundFog1X, this.backgroundFog1Y, this.backgroundFog, this.width * 2 * 2, this.height * 2);
        GL11.glScaled(1.0, 1.2, 1.2);
        EFCUtils.drawImageTransparent(this.backgroundFog2X - this.width * 2, this.backgroundFog2Y, this.backgroundFog, this.width * 2 * 2, this.height * 2);
        GL11.glDisable(3042);
        GL11.glPopMatrix();
    }

    public void initOverheadMenu() {
        int x = this.width / 2 - 173;
        int y = this.height - 29;
        int buttonWidth = 85;
        this.buttonList.add(new GuiCustomPressable(2, x + 1, y, buttonWidth + 31, 28, "Играть"));
        this.buttonList.add(new GuiCustomPressable(3, x + 33 + buttonWidth, y, buttonWidth, 28, "Поддержать"));
        this.buttonList.add(new GuiCustomPressable(4, x + 119 + buttonWidth, y, buttonWidth, 28, "Инфо"));
        this.buttonList.add(new GuiCustomPressable(5, x + 35 + 3 * buttonWidth, y, 30, 28, this.iconSettings));
        this.buttonList.add(new GuiCustomPressable(6, x + 66 + 3 * buttonWidth, y, 30, 28, this.iconPower));
        //this.buttonList.add(new GuiCustomPressable(7, 0, 50, 60, 18, "News     >>"));
        //this.buttonList.add(new GuiCustomPressable(8, 0, 69, 60, 18, "Updates >>"));
        //this.buttonList.add(new GuiCustomPressable(0, 0, 88, 60, 18, "Soon" + " >>"));
    }

    static {
        try {
            DONATE = new URI("http://www.donationalerts.ru/r/sko0ma");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}