package com.existence.mod.escapeFromChernakov.mainModule.backend.client.events;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.event.world.BlockEvent;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.06.2018
 * Time: 10:52
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class ClientMainEvents {

    @SubscribeEvent
    public void onEntityConstructing(EntityEvent.EntityConstructing event) {
    }

    @SubscribeEvent
    public void blockBreak(BlockEvent.BreakEvent e)
    {
        EntityPlayer player = e.getPlayer();

        if(player.ticksExisted < 60)
        {
            e.setCanceled(true);
        }

        if(player != null)
        {
            boolean shouldCancel = true;
            /*if(e.block == BlockRegistry.placeableNails || e.block == BlockRegistry.placeableMine)
            {
                TileEntity tile = e.world.getTileEntity(e.x, e.y, e.z);

                if(tile instanceof TilePlaceable)
                {
                    TilePlaceable tp = (TilePlaceable) tile;
                    if(tp.owneruuid.contains(player.getUniqueID().toString()))
                    {
                        shouldCancel = false;
                    }else{
                        IChatComponent test = new ChatComponentText(EnumChatFormatting.RED.toString()+"[EFC] This placeable block isn't yours.");
                        player.addChatMessage(test);
                    }
                }
            }*/

            if(!player.capabilities.isCreativeMode && shouldCancel)
            {
                if(e.isCancelable())
                {
                    IChatComponent test = new ChatComponentText(EnumChatFormatting.RED.toString()+"[EFC] Can't destroy this block.");
                    player.addChatMessage(test);
                    e.setCanceled(true);
                }
            }
        }
    }

    @SubscribeEvent
    public void placeBlock(BlockEvent.PlaceEvent e)
    {
        if(e.player.ticksExisted < 60)
        {
            e.setCanceled(true);
        }

        boolean cancel = false;

        /*if(e.block == BlockRegistry.placeableNails || e.block == BlockRegistry.placeableMine)
        {
            Block block = e.world.getBlock(e.x, e.y-1, e.z);

            if(block.isOpaqueCube() && block.isCollidable() && block.isNormalCube() && block.renderAsNormalBlock())
            {
                return;
            }else{
                cancel = true;
            }
        }*/

        if((!e.player.capabilities.isCreativeMode) || cancel)
        {
            if(e.isCancelable())
            {
                IChatComponent test = new ChatComponentText(EnumChatFormatting.RED.toString()+"[EFC] Can't place block.");
                e.player.addChatMessage(test);
                e.setCanceled(true);
            }
        }
    }
}
