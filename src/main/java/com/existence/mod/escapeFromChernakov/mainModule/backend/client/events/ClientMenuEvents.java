package com.existence.mod.escapeFromChernakov.mainModule.backend.client.events;

import com.existence.mod.escapeFromChernakov.mainModule.backend.client.gui.mainMenu.EFCParalaxMainMenu;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.client.event.GuiOpenEvent;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 18.06.2018
 * Time: 16:03
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
@SideOnly(Side.CLIENT)
public class ClientMenuEvents {

    Minecraft mc = Minecraft.getMinecraft();
    public static GuiScreen currentScreen = null;
    public static GuiScreen lastScreen = null;

    @SubscribeEvent
    public void onGuiOpenEvent(GuiOpenEvent event) {
        GuiScreen scr = mc.currentScreen;

        if (event.gui instanceof GuiMainMenu) {
            event.gui = new EFCParalaxMainMenu();
            if (currentScreen != scr) {
                lastScreen = currentScreen;
                currentScreen = scr;
            }
        }
    }
}
