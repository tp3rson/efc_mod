package com.existence.mod.escapeFromChernakov.mainModule;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 08.05.2018
 * Time: 14:32
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class EFCReference {

    public static final String MOD_ID = "escapefromchernakov";
    public static final String MOD_NAME = "Escape From Chernakov";
    public static final String VERSION = "@VERSION@";
    private static final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    public static final String VERSION_DATE = dateFormat.format(new Date());
    public static final String GUI_FACTORY_CLASS = "com.existence.mod.escapeFromChernakov.mainModule.backend.client.gui.GuiConfigFactory";
    public static final String DEPEDNENCIES = "putDependenciesHere";
    public static final String CLIENT_PROXY_CLASS = "matteroverdrive.commonProxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "matteroverdrive.commonProxy.CommonProxy";
    public static final String CHANNEL_NAME = MOD_ID + "_channel";
    public static final String CHANNEL_WEAPONS_NAME = MOD_ID + "_channel:weapons";
    public static final String CHANNEL_GUI_NAME = MOD_ID + "_channel:gui";
}
