package com.existence.mod.escapeFromChernakov.mainModule.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.05.2018
 * Time: 11:46
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class CommonProxy {

    public static void preInit(FMLPreInitializationEvent event) {
    }

    public static void init(FMLInitializationEvent event) {
    }

    public EntityPlayer getPlayerEntity(MessageContext ctx) {

        return ctx.getServerHandler().playerEntity;
    }

    public static void registerGuiEvents() {
        ClientProxy.registerGuiEvents();
    }
}
