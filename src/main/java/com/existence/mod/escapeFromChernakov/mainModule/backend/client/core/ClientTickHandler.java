package com.existence.mod.escapeFromChernakov.mainModule.backend.client.core;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 10.05.2018
 * Time: 20:48
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class ClientTickHandler {

    public static int ticksInGame = 0;
    public static float partialTicks = 0;
    public static float delta = 0;
    public static float total = 0;

    private void calcDelta() {
        float oldTotal = total;
        total = ticksInGame + partialTicks;
        delta = total - oldTotal;
    }

    @SubscribeEvent
    public void renderTick(TickEvent.RenderTickEvent event) {
        if (event.phase == TickEvent.Phase.START)
            partialTicks = event.renderTickTime;
        else {
            calcDelta();
        }
    }
}
