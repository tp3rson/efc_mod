package com.existence.mod.escapeFromChernakov.mainModule.backend.client.events;

import com.existence.mod.escapeFromChernakov.util.TextureMap;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 15.05.2018
 * Time: 11:39
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
@SideOnly(Side.CLIENT)
public class ClientGuiEvents {

    public Minecraft mc;
    protected static final RenderItem itemRenderer = new RenderItem();
    protected float zLevel;
    private float foodLevel;
    private long boneFadeTimer;
    private boolean boneOn;

    public static GuiScreen currentScreen = null;
    public static GuiScreen lastScreen = null;

    public ClientGuiEvents(Minecraft mc) {
        this.mc = mc;
    }

    @SubscribeEvent(
            receiveCanceled = true,
            priority = EventPriority.HIGHEST
    )
    public void onInGameUI(RenderGameOverlayEvent e) {
        long time = System.currentTimeMillis();
        if (this.boneFadeTimer == 0L || this.boneFadeTimer < time) {
            this.boneFadeTimer = time + 1500L;
            this.boneOn = !this.boneOn;
        }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        //this.drawMainUI(e);
        //this.drawFriendsPopUp(e);
        if (!mc.thePlayer.capabilities.isCreativeMode) {
            this.drawHotBarUI(e);
        }
        if (this.mc.thePlayer.getItemInUse() != null && this.mc.gameSettings.thirdPersonView == 0 && !(this.mc.thePlayer.getItemInUse().getItem() instanceof ItemSword)) {
            this.drawItemInUseProgress(e);
        }

        //this.drawBloodStatusUI(e);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }

    private void drawHotBarUI(RenderGameOverlayEvent e) {
        if (e.type == RenderGameOverlayEvent.ElementType.EXPERIENCE || e.type == RenderGameOverlayEvent.ElementType.HEALTH || e.type == RenderGameOverlayEvent.ElementType.FOOD || e.type == RenderGameOverlayEvent.ElementType.HOTBAR || e.type == RenderGameOverlayEvent.ElementType.ARMOR || e.type == RenderGameOverlayEvent.ElementType.DEBUG) {
            e.setCanceled(true);
        }

        if (e.type == RenderGameOverlayEvent.ElementType.ALL) {
            GL11.glPushMatrix();
            GL11.glTranslatef((float) (e.resolution.getScaledWidth() / 2 - 50 + 2), (float) (e.resolution.getScaledHeight() - 19), 0.0F);
            Tessellator t = Tessellator.instance;
            GL11.glBindTexture(3553, TextureMap.get("ui_Selected_slot").getGlTextureId());
            if (this.mc.thePlayer.inventory.currentItem >= 0 && this.mc.thePlayer.inventory.currentItem <= 4) {
                t.startDrawingQuads();
                t.addVertexWithUV((double) (-2 + this.mc.thePlayer.inventory.currentItem * 20), -2.0D, 0.0D, 0.0D, 0.0D);
                t.addVertexWithUV((double) (-2 + this.mc.thePlayer.inventory.currentItem * 20), 18.0D, 0.0D, 0.0D, 1.0D);
                t.addVertexWithUV((double) (18 + this.mc.thePlayer.inventory.currentItem * 20), 18.0D, 0.0D, 1.0D, 1.0D);
                t.addVertexWithUV((double) (18 + this.mc.thePlayer.inventory.currentItem * 20), -2.0D, 0.0D, 1.0D, 0.0D);
                t.draw();
            }

            if (this.mc.thePlayer.inventory.currentItem > 4 && this.mc.thePlayer.inventory.currentItem < 7) {
                this.mc.thePlayer.inventory.currentItem = 0;
            }

            if (this.mc.thePlayer.inventory.currentItem >= 7 && this.mc.thePlayer.inventory.currentItem <= 9) {
                this.mc.thePlayer.inventory.currentItem = 4;
            }

            for (int i = 0; i < 5; ++i) {
                GL11.glBindTexture(3553, TextureMap.get("ui_slot").getGlTextureId());
                GL11.glEnable(3042);
                RenderHelper.disableStandardItemLighting();
                t.startDrawingQuads();
                t.addVertexWithUV((double) (0 + i * 20), 0.0D, 0.0D, 0.0D, 0.0D);
                t.addVertexWithUV((double) (0 + i * 20), 16.0D, 0.0D, 0.0D, 1.0D);
                t.addVertexWithUV((double) (16 + i * 20), 16.0D, 0.0D, 1.0D, 1.0D);
                t.addVertexWithUV((double) (16 + i * 20), 0.0D, 0.0D, 1.0D, 0.0D);
                t.draw();
                this.renderInventorySlot(i, i * 20, 0, 0.0F);
            }

            RenderHelper.disableStandardItemLighting();
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            GL11.glPopMatrix();
        }

    }

    /**
     * Отрисовка прогресс-бара использования какого либо предмета, который имеет время использования.
     *
     * @param renderGameOverlayEvent
     */
    private void drawItemInUseProgress(RenderGameOverlayEvent renderGameOverlayEvent) {
        if (renderGameOverlayEvent.type == RenderGameOverlayEvent.ElementType.ALL) {
            float currentUse = (float) this.mc.thePlayer.getItemInUseDuration();
            float maxUse = (float) this.mc.thePlayer.getItemInUse().getMaxItemUseDuration();
            GL11.glPushMatrix();

            final ScaledResolution res = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
            final FontRenderer fr = mc.fontRenderer;
            final String text = "Использование " + I18n.format(this.mc.thePlayer.inventory.getCurrentItem().getDisplayName()); //TODO: сделать скалябельным
            final int width2 = fr.getStringWidth(text);
            final int x = (res.getScaledWidth() - width2) / 2;
            final int y = res.getScaledHeight() - 29 - fr.FONT_HEIGHT;
            Gui.drawRect(x - 2, y - 3, x + width2 + 2, y + fr.FONT_HEIGHT + 2, 1426063360);
            fr.drawString(text, x, y, 16777215);

            Tessellator t = Tessellator.instance;
            GL11.glEnable(3553);
            GL11.glEnable(3042);
            GL11.glBindTexture(3553, TextureMap.get("itemUseProgressBar").getGlTextureId());
            GL11.glTranslatef((float) (renderGameOverlayEvent.resolution.getScaledWidth() / 2 - 30), (float) (renderGameOverlayEvent.resolution.getScaledHeight() - 49), 0.0F);
            t.startDrawingQuads();
            GL11.glScalef(0.3f, 0.3f, 0.3f);
            t.addVertexWithUV(0.0D, 0.0D, 0.0D, 0.0D, 0.0D);
            t.addVertexWithUV(0.0D, 16.0D, 0.0D, 0.0D, 1.0D);
            t.addVertexWithUV(200.0D, 16.0D, 0.0D, 1.0D, 1.0D);
            t.addVertexWithUV(200.0D, 0.0D, 0.0D, 1.0D, 0.0D);
            t.draw();
            GL11.glDisable(3553);
            t.startDrawingQuads();
            t.addVertexWithUV(5.0D, 5.0D, 0.0D, 0.0D, 0.0D);
            t.addVertexWithUV(5.0D, 11.0D, 0.0D, 0.0D, 1.0D);
            t.addVertexWithUV((double) (5.0F + 190.0F * (currentUse / maxUse)), 11.0D, 0.0D, 1.0D, 1.0D);
            t.addVertexWithUV((double) (5.0F + 190.0F * (currentUse / maxUse)), 5.0D, 0.0D, 1.0D, 0.0D);
            t.draw();
            GL11.glEnable(3553);
            GL11.glPopMatrix();
        }
    }

    /**
     * @param i
     * @param p_73832_2_
     * @param p_73832_3_
     * @param p_73832_4_
     */
    protected void renderInventorySlot(int i, int p_73832_2_, int p_73832_3_, float p_73832_4_) {
        ItemStack itemstack = this.mc.thePlayer.inventory.mainInventory[i];
        if (itemstack != null) {
            float f1 = (float) itemstack.animationsToGo - p_73832_4_;
            if (f1 > 0.0F) {
                GL11.glPushMatrix();
                float f2 = 1.0F + f1 / 5.0F;
                GL11.glTranslatef((float) (p_73832_2_ + 8), (float) (p_73832_3_ + 12), 0.0F);
                GL11.glScalef(1.0F / f2, (f2 + 1.0F) / 2.0F, 1.0F);
                GL11.glTranslatef((float) (-(p_73832_2_ + 8)), (float) (-(p_73832_3_ + 12)), 0.0F);
            }

            //itemRenderer.renderItemIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), itemstack, p_73832_2_, p_73832_3_);
            if (f1 > 0.0F) {
                GL11.glPopMatrix();
            }

            //itemRenderer.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), itemstack, p_73832_2_, p_73832_3_);
            itemRenderer.renderItemAndEffectIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), itemstack, p_73832_2_, p_73832_3_);
            itemRenderer.renderItemOverlayIntoGUI(this.mc.fontRenderer, this.mc.getTextureManager(), itemstack, p_73832_2_, p_73832_3_);
            //this.zLevel = 0.0F;
            //itemRenderer.zLevel = 0.0F;
        }
    }

}
