package com.existence.mod.escapeFromChernakov.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

import java.awt.*;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 17.06.2018
 * Time: 12:43
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class EFCUtils {

    /**
     * Метод для отрисовки изображения с ЛЮБЫМ размером текстуры;
     * posX, posY - позиция width, height - размер
     */
    public static void drawCompleteImage(int posX, int posY, int width, int height) {
        GL11.glPushMatrix();

        GL11.glTranslatef(posX, posY, 0.0F);
        GL11.glBegin(7);

        GL11.glTexCoord2f(0.0F, 0.0F);
        GL11.glVertex3f(0.0F, 0.0F, 0.0F);
        GL11.glTexCoord2f(0.0F, 1.0F);
        GL11.glVertex3f(0.0F, height, 0.0F);
        GL11.glTexCoord2f(1.0F, 1.0F);
        GL11.glVertex3f(width, height, 0.0F);
        GL11.glTexCoord2f(1.0F, 0.0F);
        GL11.glVertex3f(width, 0.0F, 0.0F);
        GL11.glEnd();

        GL11.glPopMatrix();
    }

    public static int menuTheme = 15987699;
    //private static CharacterPlayerRender characterRenderer;

    public static void renderCenteredTextThemed(String text, int posX, int posY) {
        Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(text, posX - Minecraft.getMinecraft().fontRenderer.getStringWidth(text) / 2, posY, menuTheme);
    }

    public static void renderTextThemed(String text, int posX, int posY) {
        Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(text, posX, posY, menuTheme);
    }

    public static void drawImage(double x, double y, ResourceLocation image, double width, double height) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        GL11.glHint((int) 3153, (int) 4353);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 3042);
        GL11.glDisable((int) 2832);
    }

    public static void drawImageTransparent(double x, double y, ResourceLocation image, double width, double height) {
        Minecraft.getMinecraft().renderEngine.bindTexture(image);
        Tessellator tess = Tessellator.instance;
        GL11.glEnable((int) 3042);
        GL11.glEnable((int) 2832);
        tess.setColorRGBA_F(2.0f, 0.0f, 0.0f, 0.9f);
        tess.startDrawingQuads();
        tess.addVertexWithUV(x, y + height, 0.0, 0.0, 1.0);
        tess.addVertexWithUV(x + width, y + height, 0.0, 1.0, 1.0);
        tess.addVertexWithUV(x + width, y, 0.0, 1.0, 0.0);
        tess.addVertexWithUV(x, y, 0.0, 0.0, 0.0);
        tess.draw();
        GL11.glDisable((int) 2832);
        GL11.glDisable((int) 3042);
    }

    //public static void renderPlayerModel(Minecraft minecraft, EntityPlayer player, int posX, int posY, float scale, float rotation) {
    //    RenderManager.field_78727_a.field_147941_i = player;
    //    RenderManager.field_78727_a.field_78724_e = minecraft.func_110434_K();
    //    if (characterRenderer == null) {
    //        characterRenderer = new CharacterPlayerRender();
    //        characterRenderer.func_76976_a(RenderManager.field_78727_a);
    //        return;
    //    }
    //    GL11.glEnable((int)2903);
    //    GL11.glPushMatrix();
    //    GL11.glTranslatef((float)posX, (float)posY, (float)50.0f);
    //    GL11.glScalef((float)(- scale), (float)scale, (float)scale);
    //    GL11.glRotatef((float)180.0f, (float)0.0f, (float)0.0f, (float)1.0f);
    //    GL11.glEnable((int)2884);
    //    GL11.glEnable((int)3008);
    //    GL11.glEnable((int)2929);
    //    float f2 = player.field_70761_aq;
    //    float f3 = player.field_70177_z;
    //    float f4 = player.field_70125_A;
    //    GL11.glRotatef((float)0.0f, (float)0.0f, (float)1.0f, (float)0.0f);
    //    RenderHelper.func_74519_b();
    //    GL11.glRotatef((float)(-110.0f + player.field_70761_aq + (float)(Mouse.getX() / 20 + 90)), (float)0.0f, (float)1.0f, (float)0.0f);
    //    player.field_70761_aq = 0.0f;
    //    player.field_70177_z = 0.0f;
    //    player.field_70125_A = 50.0f - (float)(Mouse.getY() / 8);
    //    player.field_70759_as = player.field_70761_aq - (float)(Mouse.getX() / 8 + 240);
    //    GL11.glTranslatef((float)0.0f, (float)player.field_70129_M, (float)0.0f);
    //    RenderManager.field_78727_a.field_78732_j = 180.0f;
    //    characterRenderer.func_76986_a((EntityLivingBase)player, 0.0, 0.0, 0.0, 0.0f, 0.625f);
    //    player.field_70761_aq = f2;
    //    player.field_70177_z = f3;
    //    player.field_70125_A = f4;
    //    GL11.glPopMatrix();
    //    RenderHelper.func_74518_a();
    //    GL11.glDisable((int)32826);
    //    OpenGlHelper.func_77473_a((int)OpenGlHelper.field_77476_b);
    //    GL11.glDisable((int)3553);
    //    OpenGlHelper.func_77473_a((int)OpenGlHelper.field_77478_a);
    //}

    public static void renderPlayerHead(String playerName, int xPos, int yPos) {
        ResourceLocation resourceLocation = AbstractClientPlayer.locationStevePng;
        if (playerName.length() > 0) {
            resourceLocation = AbstractClientPlayer.getLocationSkin((String) playerName);
            AbstractClientPlayer.getDownloadImageSkin((ResourceLocation) resourceLocation, (String) playerName);
        }
        GL11.glPushMatrix();
        EFCUtils.drawRect(xPos - 1, yPos - 1, 20, 21, new Color(0, 0, 0, 127));
        Minecraft.getMinecraft().renderEngine.bindTexture(resourceLocation);
        GL11.glTranslated((double) xPos, (double) yPos, (double) 30.0);
        GL11.glScaled((double) 0.75, (double) 0.39, (double) 0.0);
        double scale = 0.75;
        GL11.glScaled((double) scale, (double) scale, (double) scale);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        RenderHelper.disableStandardItemLighting();
        Minecraft.getMinecraft().ingameGUI.drawTexturedModalRect(0, 0, 32, 64, 32, 64);
        Minecraft.getMinecraft().ingameGUI.drawTexturedModalRect(0, 0, 160, 64, 32, 64);
        GL11.glPopMatrix();
    }

    public static void drawTextWithOutline(String text, int x, int y, int color) {
        Minecraft mc = Minecraft.getMinecraft();
        FontRenderer fr = mc.fontRenderer;
        fr.drawString(text, x - 1, y + 1, 0);
        fr.drawString(text, x, y + 1, 0);
        fr.drawString(text, x + 1, y + 1, 0);
        fr.drawString(text, x - 1, y, 0);
        fr.drawString(text, x + 1, y, 0);
        fr.drawString(text, x - 1, y - 1, 0);
        fr.drawString(text, x, y - 1, 0);
        fr.drawString(text, x + 1, y - 1, 0);
        fr.drawString(text, x, y, color);
    }

    public static void drawRect(int xStart, int yStart, int xEnd, int yEnd, Color color) {
        float r = (float) color.getRed() / 255.0f;
        float g = (float) color.getGreen() / 255.0f;
        float b = (float) color.getBlue() / 255.0f;
        float a = (float) color.getAlpha() / 255.0f;
        Tessellator tessellator = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glDisable((int) 3553);
        GL11.glColor4f((float) r, (float) g, (float) b, (float) a);
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) xStart, (double) (yStart + yEnd), 0.0);
        tessellator.addVertex((double) (xStart + xEnd), (double) (yStart + yEnd), 0.0);
        tessellator.addVertex((double) (xStart + xEnd), (double) yStart, 0.0);
        tessellator.addVertex((double) xStart, (double) yStart, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
        GL11.glDisable((int) 3042);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glPopMatrix();
    }

    public static void drawRectWithShadow(int x, int y, int width, int height, Color color, int alpha) {
        EFCUtils.drawRect(x - 1, y - 1, width + 2, height + 2, new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha / 2));
        EFCUtils.drawRect(x, y, width, height, new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha));
    }

    public static boolean isInBox(int x, int y, int width, int height, int checkX, int checkY) {
        return checkX >= x && checkY >= y && checkX <= x + width && checkY <= y + height;
    }

    public static void drawTexturedQuadFit(int x, int y, int width, int height, float zLevel) {
        Tessellator t = Tessellator.instance;
        t.startDrawingQuads();
        t.addVertexWithUV((double) x, (double) (y + height), (double) zLevel, 0.0, 1.0);
        t.addVertexWithUV((double) (x + width), (double) (y + height), (double) zLevel, 1.0, 1.0);
        t.addVertexWithUV((double) (x + width), (double) y, (double) zLevel, 1.0, 0.0);
        t.addVertexWithUV((double) x, (double) y, (double) zLevel, 0.0, 0.0);
        t.draw();
    }

    public static void drawRectTransparent(int x, int y, int width, int height, String colorString, float alpha) {
        Color color = Color.decode(colorString);
        float red = (float) color.getRed() / 255.0f;
        float green = (float) color.getGreen() / 255.0f;
        float blue = (float) color.getBlue() / 255.0f;
        EFCUtils.drawRectTransparent(x, y, x + width, y + height, red, green, blue, alpha);
    }

    public static void drawRectTransparent(int xStart, int yStart, int xEnd, int yEnd, int rgb) {
        Tessellator tessellator = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glDisable((int) 3553);
        GL11.glColor3b((byte) ((byte) (rgb >> 16)), (byte) ((byte) (rgb >> 8 & 255)), (byte) ((byte) (rgb & 255)));
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) xStart, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yStart, 0.0);
        tessellator.addVertex((double) xStart, (double) yStart, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glPopMatrix();
    }

    public static void drawRectTransparent(int xStart, int yStart, int xEnd, int yEnd, float r, float g, float b, float alpha) {
        Tessellator tessellator = Tessellator.instance;
        GL11.glPushMatrix();
        GL11.glEnable((int) 3042);
        GL11.glBlendFunc((int) 770, (int) 771);
        GL11.glDisable((int) 3553);
        GL11.glColor4f((float) r, (float) g, (float) b, (float) alpha);
        tessellator.startDrawingQuads();
        tessellator.addVertex((double) xStart, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yEnd, 0.0);
        tessellator.addVertex((double) xEnd, (double) yStart, 0.0);
        tessellator.addVertex((double) xStart, (double) yStart, 0.0);
        tessellator.draw();
        GL11.glEnable((int) 3553);
        GL11.glDisable((int) 3042);
        GL11.glColor4f((float) 1.0f, (float) 1.0f, (float) 1.0f, (float) 1.0f);
        GL11.glPopMatrix();
    }
}
