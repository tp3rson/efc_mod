package com.existence.mod.escapeFromChernakov.util.internal;

import com.google.common.util.concurrent.AbstractListeningExecutorService;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 0:56
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public abstract class SchedulerBase extends AbstractListeningExecutorService {

    protected abstract void tick();

}
