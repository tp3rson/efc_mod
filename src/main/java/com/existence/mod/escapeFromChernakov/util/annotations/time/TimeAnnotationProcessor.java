/*package com.existence.mod.escapeFromChernakov.util.annotations.time;

import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.model.JavacElements;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCBlock;
import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCExpressionStatement;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCStatement;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.List;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.Elements;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 18.06.2018
 * Time: 18:39
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
/*@SupportedAnnotationTypes(value = {TimeAnnotationProcessor.ANNOTATION_TYPE})
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class TimeAnnotationProcessor extends AbstractProcessor {

    /**
     * Класс-обработчик расширяющий AbstractProcessor.
     * В этом классе добавляет сохранение времени перед кодом метода,
     *     сам код метода копируется в блок try-finally, а в блоке finally
     *     вычисляется затраченное методом время и выводится в консоль.
     */

/**
 * Для того чтобы компилятор java использовал наш управляющий класс,
 * нужно создать файл META-INF/javax.annotation.processing.Processor,
 * в котором должна быть прописана следующая строка:
 * annotations.time.TimeAnnotationProcessor
 * <p>
 * После этого собираем все наши файлы в annotations.jar и добавляем его в classpath к любому проекту.
 * <p>
 * Теперь, чтобы посчитать время выполнения метода,
 * достаточно добавить к этому методу аннотацию Time и после его выполнения в консоль будет выведено затраченное методом время:
 *
 * @Time(format="method time: %s ms")
 * public void start() {
 * Thread.sleep(1000);
 * }
 */

    /*public static final String ANNOTATION_TYPE = "com.existence.mod.escapeFromChernakov.util.annotations.time.Time";
    private JavacProcessingEnvironment javacProcessingEnv;
    private TreeMaker maker;

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        this.javacProcessingEnv = (JavacProcessingEnvironment) processingEnv;
        this.maker = TreeMaker.instance(javacProcessingEnv.getContext());
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        if (annotations == null || annotations.isEmpty()) {
            return false;
        }

        final Elements elements = javacProcessingEnv.getElementUtils();
        final TypeElement annotation = elements.getTypeElement(ANNOTATION_TYPE);

        if (annotation != null) {
            //Выбираем все элементы у которых стоит наша аннотация
            final Set<? extends Element> methods = roundEnvironment.getElementsAnnotatedWith(annotation);
            JavacElements utils = javacProcessingEnv.getElementUtils();
            for (final Element m : methods) {
                Time time = m.getAnnotation(Time.class);
                if (time != null) {
                    JCTree blockNode = utils.getTree(m);
                    //Нужны только описания методов без лишнего мусора
                    if (blockNode instanceof  JCMethodDecl) {
                        //Получаем содержимое метода
                        final List<JCStatement> statements = ((JCMethodDecl) blockNode).body.stats;

                        //Новое тело метода
                        List<JCStatement> newStatements = List.nil();
                        //Добавляем в начало метода сохранение текущего времени
                        JCVariableDecl variableDecl = makeTimeStartVar(maker, utils, time);
                        newStatements = newStatements.append(variableDecl);

                        // Создаём тело блока try, копируем в него оригинальное содержимое метода
                        List<JCStatement> tryBlock = List.nil();
                        for (JCStatement statement : statements) {
                            tryBlock = tryBlock.append(statement);
                        }

                        // Создаём тело блока finally, добавляем в него вывод затраченного времени
                        JCBlock finalizer = makePrintBlock(maker, utils, time, variableDecl);
                        JCStatement stat = maker.Try(maker.Block(0, tryBlock), List.<JCCatch>nil(), finalizer);
                        newStatements = newStatements.append(stat);

                        // Заменяем старый код метода на новый
                        ((JCMethodDecl) blockNode).body.stats = newStatements;
                    }
                }
            }

            return true;
        }

        return false;
    }

    private JCExpression makeCurrentTime (TreeMaker maker, JavacElements utils, Time time) {
        //Создаем вызов System.nanoTime или System.currentTimeMillis
        JCExpression jcExpression = maker.Ident(utils.getName("System"));
        String methodName;

        switch (time.interval()) {
            case NANOSECOND:
                methodName = "nanoTime";
                break;

            default:
                methodName = "currentTimeMillis";
                break;
        }

        jcExpression = maker.Select(jcExpression, utils.getName(methodName));
        return maker.Apply(List.<JCExpression>nil(), jcExpression, List.<JCExpression>nil());
    }

    protected JCVariableDecl makeTimeStartVar(TreeMaker maker, JavacElements utils, Time time) {
        // Создаём финальную переменную для хранения времени старта. Имя переменной в виде time_start_{random}
        JCExpression currentTime = makeCurrentTime(maker, utils, time);
        String fieldName = fieldName = "time_start_" + (int) (Math.random() * 10000);
        return maker.VarDef(maker.Modifiers(Flags.FINAL), utils.getName(fieldName), maker.TypeIdent(TypeTag.LONG), currentTime);
    }

    protected JCBlock makePrintBlock(TreeMaker maker, JavacElements utils, Time time, JCVariableDecl var) {
        // Создаём вызов System.out.println
        JCExpression printlnExpression = maker.Ident(utils.getName("System"));
        printlnExpression = maker.Select(printlnExpression, utils.getName("out"));
        printlnExpression = maker.Select(printlnExpression, utils.getName("println"));

        // Создаём блок вычисления затраченного времени (currentTime - startTime)
        JCExpression currentTime = makeCurrentTime(maker, utils, time);
        JCExpression elapsedTime = maker.Binary(JCTree.Tag.MINUS, currentTime, maker.Ident(var.name));

        // Форматируем результат
        JCExpression formatExpression = maker.Ident(utils.getName("String"));
        formatExpression = maker.Select(formatExpression, utils.getName("format"));

        // Собираем все кусочки вместе
        List<JCExpression> formatArgs = List.nil();
        formatArgs.append(maker.Literal(time.format()));
        formatArgs.append(elapsedTime);

        JCExpression format = maker.Apply(List.<JCExpression>nil(), formatExpression, formatArgs);

        List<JCExpression> printlnArgs = List.nil();
        printlnArgs.append(format);

        JCExpression print = maker.Apply(List.<JCExpression>nil(), printlnExpression, printlnArgs);
        JCExpressionStatement stmt = maker.Exec(print);

        List<JCStatement> stmts = List.nil();
        stmts.append(stmt);

        return maker.Block(0, stmts);
    }
}*/