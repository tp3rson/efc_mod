package com.existence.mod.escapeFromChernakov.chatModule.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.IChatComponent.Serializer;

public class MsgChatWrapperClient implements IMessage {

    private IChatComponent msg;
    private String target;


    public MsgChatWrapperClient() {
    }

    public MsgChatWrapperClient(String msg, String target) {
        this((IChatComponent) (new ChatComponentText(msg)), target);
    }

    public MsgChatWrapperClient(IChatComponent msg, String target) {
        this.msg = msg;
        this.target = target;
    }

    public IChatComponent getMsg() {
        return this.msg;
    }

    public void setMsg(IChatComponent msg) {
        this.msg = msg;
    }

    public String getTarget() {
        return this.target;
    }

    public void fromBytes(ByteBuf buf) {
        this.msg = Serializer.func_150699_a(ByteBufUtils.readUTF8String(buf));
        this.target = ByteBufUtils.readUTF8String(buf);
    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, Serializer.func_150696_a(this.msg));
        ByteBufUtils.writeUTF8String(buf, this.target);
    }

    public static class C01ChatExt extends C01PacketChatMessage {

        private String msg;


        public C01ChatExt(String msg) {
            this.msg = msg;
        }

        public String func_149439_c() {
            return this.msg;
        }

        public boolean hasPriority() {
            return false;
        }
    }

    public static class Handler
            implements IMessageHandler<MsgChatWrapperClient, IMessage> {
        public IMessage onMessage(MsgChatWrapperClient message, MessageContext ctx) {
            ctx.getServerHandler().processChatMessage((C01PacketChatMessage) new C01ChatExt(message.msg.getUnformattedTextForChat()));
            return null;
        }
    }
}
