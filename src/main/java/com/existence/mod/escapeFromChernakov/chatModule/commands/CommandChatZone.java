package com.existence.mod.escapeFromChernakov.chatModule.commands;

import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;

public class CommandChatZone extends CommandBase {

    public String getCommandName() {
        return "chatzone";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/chatzone <channel> <start|stop|remove>";
    }

    public void processCommand(ICommandSender sender, String[] data) {
        if (data.length >= 1 && data[0].startsWith("#")) {
            if (sender instanceof EntityPlayerMP && !((EntityPlayerMP) sender).mcServer.getConfigurationManager().func_152596_g(((EntityPlayerMP) sender).getGameProfile())) {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Normal players can\'t create new chat zones."), sender);
            } else {
                if (sender instanceof EntityPlayerMP) {
                    ReturnStatus stat;
                    if (data[1].equals("start")) {
                        stat = ChannelHandler.INSTANCE.startZone(data[0], (EntityPlayerMP) sender);
                        NetworkHelper.sendSystemMessage(stat, (EntityPlayerMP) sender);
                    } else if (data[1].equals("stop")) {
                        stat = ChannelHandler.INSTANCE.stopZone(data[0], (EntityPlayerMP) sender);
                        NetworkHelper.sendSystemMessage(stat, (EntityPlayerMP) sender);
                    } else {
                        if (!data[1].equals("remove")) {
                            throw new WrongUsageException(this.getCommandUsage(sender), new Object[0]);
                        }

                        stat = ChannelHandler.INSTANCE.removeZone(data[0], (EntityPlayerMP) sender);
                        NetworkHelper.sendSystemMessage(stat, (EntityPlayerMP) sender);
                    }
                }

            }
        } else {
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, this.getCommandUsage(sender)), sender);
        }
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
