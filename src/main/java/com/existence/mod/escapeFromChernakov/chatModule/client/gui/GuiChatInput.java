package com.existence.mod.escapeFromChernakov.chatModule.client.gui;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.api.PrattleMouseEvent;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChatWrapperClient;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.utils.StringHelper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.*;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import java.util.Iterator;
import java.util.Set;

@SideOnly(Side.CLIENT)
public class GuiChatInput extends GuiChat implements GuiYesNoCallback {

    private boolean flag;
    private GuiTab tabAdd;
    private int lx;
    private int ly;
    private int lw;
    private int lh;
    private int loffset = 0;
    private int lnitems = 0;
    private int lstep = 8;


    public GuiChatInput() {
    }

    public GuiChatInput(String msg) {
        super(msg);
    }

    private GuiChatText getChatText() {
        return (GuiChatText) super.mc.ingameGUI.getChatGUI();
    }

    public void initGui() {
        super.initGui();
        super.inputField.setMaxStringLength(Talkative.maxChatChars);
        this.setButtons();
        this.lx = 328;
        this.ly = super.height - 190;
        this.lw = 0;
        this.lh = 162;
    }

    private void setButtons() {
        super.buttonList.clear();
        Iterator i$ = this.getChatText().getChannels().iterator();

        while (i$.hasNext()) {
            String channel = (String) i$.next();
            super.buttonList.add(this.getNewTab(channel));
        }

        this.tabAdd = this.getNewTab("+");
    }

    private GuiTab getNewTab(String label) {
        int strWidth = super.mc.fontRenderer.getStringWidth(label) + 16;
        boolean unread = false;
        int unread1 = ((GuiChatText) super.mc.ingameGUI.getChatGUI()).getUnread(label);
        if (unread1 > 0) {
            strWidth += super.mc.fontRenderer.getStringWidth(" [" + unread1 + "]");
        }

        int xpos = 0;

        Object o;
        for (Iterator tab = super.buttonList.iterator(); tab.hasNext(); xpos += ((GuiButton) o).width) {
            o = tab.next();
        }

        GuiTab tab1 = new GuiTab(super.buttonList.size() + 1, 2 + xpos, super.height - 26, strWidth, 12, label);
        if (this.getChatText().getChannelDisplay().equals(label)) {
            tab1.setSelected(true);
        }

        return tab1;
    }

    public void func_146403_a(String msg) {
        super.mc.ingameGUI.getChatGUI().addToSentMessages(msg);
        if (ClientCommandHandler.instance.executeCommand(super.mc.thePlayer, msg) == 0) {
            if (msg.startsWith("/")) {
                NetworkHelper.sendToServer(new MsgChatWrapperClient(msg, ""));
            } else {
                String channel = StringHelper.getTextWithoutFormattingCodes(this.getChatText().getChannelDisplay());
                NetworkHelper.sendToServer(new MsgChatWrapperClient("/pm " + channel + " " + msg, channel));
            }

        }
    }

    public void drawScreen(int x, int y, float z) {
        if (this.getChatText().isDirty()) {
            this.setButtons();
            this.getChatText().setDirty(false);
        }

        Set usernames = this.getChatText().getUserList();
        int bgtop;
        if (usernames != null && usernames.size() > 0) {
            this.lw = this.getChatText().getUserWidth() + 8;
            ScaledResolution fontHeight = new ScaledResolution(super.mc, super.mc.displayWidth, super.mc.displayHeight);
            drawRect(this.lx, this.ly, this.lx + this.lw, this.ly + this.lh, Integer.MIN_VALUE);
            GL11.glEnable(3089);
            GL11.glScissor(328 * fontHeight.getScaleFactor(), 28 * fontHeight.getScaleFactor(), (this.getChatText().getUserWidth() + 8) * fontHeight.getScaleFactor(), 162 * fontHeight.getScaleFactor());
            bgtop = 0;

            for (Iterator bgbot = this.getChatText().getUserList().iterator(); bgbot.hasNext(); ++bgtop) {
                String guiChat = (String) bgbot.next();
                super.fontRendererObj.drawStringWithShadow(guiChat, 332, bgtop * 10 + super.height - 190 + 4 + this.loffset, -1);
            }

            GL11.glDisable(3089);
            byte var13 = 4;
            byte var16 = 8;
            int msg = (this.lh - var16 + this.lnitems * -10) / this.lstep;
            int line = (int) ((float) (this.lh - var16) / (float) msg * (float) (this.loffset / this.lstep));
            drawRect(this.lx + this.lw - var13 / 2, this.ly + line, this.lx + this.lw + var13 / 2, this.ly + var16 + line, -1);
        }

        int var12 = super.mc.fontRenderer.FONT_HEIGHT;
        bgtop = this.ly + this.lh - 180;
        int var14 = Math.max(bgtop, this.ly + this.lh - this.getChatText().getReceived() * var12);
        drawRect(2, bgtop, 326, var14, Integer.MIN_VALUE);
        GuiChatText var15 = (GuiChatText) super.mc.ingameGUI.getChatGUI();
        IChatComponent var17 = var15.func_146236_a(Mouse.getX(), Mouse.getY());
        ChatLine var18 = var15.getChatLineAtCoordinates(Mouse.getX(), Mouse.getY());
        if (var17 != null && var18 != null) {
            PrattleMouseEvent.ChatHover event = new PrattleMouseEvent.ChatHover(var17, var18.func_151461_a(), Mouse.getX(), Mouse.getY());
            MinecraftForge.EVENT_BUS.post(event);
        }

        super.drawScreen(x, y, z);
    }

    protected void actionPerformed(GuiButton button) {
        if (button == this.tabAdd) {
            super.mc.displayGuiScreen(new GuiJoinChan());
        }

        if (button instanceof GuiTab) {
            Iterator i$ = super.buttonList.iterator();

            while (i$.hasNext()) {
                Object o = i$.next();
                if (o instanceof GuiTab) {
                    ((GuiTab) o).setSelected(false);
                }
            }

            ((GuiTab) button).setSelected(true);
            this.getChatText().resetUnread(button.displayString);
            this.getChatText().switchDisplayChannel(button.displayString);
        }

    }

    protected void mouseClicked(int xpos, int ypos, int which) {
        GuiChatText guiChat = (GuiChatText) super.mc.ingameGUI.getChatGUI();
        IChatComponent msg = guiChat.func_146236_a(Mouse.getX(), Mouse.getY());
        ChatLine line = guiChat.getChatLineAtCoordinates(Mouse.getX(), Mouse.getY());
        if (msg != null && line != null) {
            PrattleMouseEvent.ChatClick i$ = new PrattleMouseEvent.ChatClick(msg, line.func_151461_a(), which);
            if (MinecraftForge.EVENT_BUS.post(i$)) {
                return;
            }
        }

        if (which == 0) {
            super.mouseClicked(xpos, ypos, which);
        } else {
            Iterator i$1 = super.buttonList.iterator();

            while (i$1.hasNext()) {
                Object o = i$1.next();
                if (o != this.tabAdd && o instanceof GuiTab && ((GuiTab) o).mousePressed(super.mc, xpos, ypos)) {
                    ((GuiTab) o).mouseEvent(super.mc, xpos, ypos, which);
                }
            }
        }

    }

    public void handleMouseInput() {
        int x = Mouse.getEventX() * super.width / super.mc.displayWidth;
        int y = super.height - Mouse.getEventY() * super.height / super.mc.displayHeight - 1;
        int i = Mouse.getEventDWheel();
        if (x >= this.lx && x <= this.lx + this.lw && y >= this.ly && y <= this.ly + this.lh && i != 0) {
            if (i > 0) {
                this.loffset += this.lstep;
            } else {
                this.loffset -= this.lstep;
            }

            this.loffset = Math.min(0, this.loffset);
            this.loffset = Math.max(this.lh + 4 + (this.lnitems + 1) * -10, this.loffset);
            if ((this.lnitems + 1) * 10 < this.lh) {
                this.loffset = 0;
            }
        } else {
            super.handleMouseInput();
        }

    }
}
