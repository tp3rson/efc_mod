package com.existence.mod.escapeFromChernakov.chatModule.server;


public class ReturnStatus {

    public String msg = "";
    public Status status;


    public ReturnStatus(Status status, String msg) {
        this.msg = msg;
        this.status = status;
    }

    public ReturnStatus(Status status) {
        this.status = status;
    }

    public boolean hasMessage() {
        return !this.msg.equals("");
    }

    public boolean isOK() {
        return this.status == Status.OK;
    }

    public static enum Status {

        OK("OK", 0),
        HELP("HELP", 1),
        ERROR("ERROR", 2),
        CRITICAL("CRITICAL", 3);
        // $FF: synthetic field
        private static final Status[] $VALUES = new Status[]{OK, HELP, ERROR, CRITICAL};


        private Status(String var1, int var2) {
        }

    }
}
