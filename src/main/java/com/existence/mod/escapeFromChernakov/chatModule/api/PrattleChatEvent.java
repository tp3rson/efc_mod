package com.existence.mod.escapeFromChernakov.chatModule.api;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.util.IChatComponent;

import java.util.Map;

@Cancelable
public class PrattleChatEvent extends Event {

    private final IChatComponent originalMsg;
    public IChatComponent displayMsg;
    public final String sender;
    public final String target;


    private PrattleChatEvent(IChatComponent msg, String sender, String target) {
        this.originalMsg = msg.createCopy();
        this.displayMsg = msg.createCopy();
        this.sender = sender;
        this.target = target;
    }

    public final IChatComponent getOriginalMsg() {
        return this.originalMsg.createCopy();
    }

    // $FF: synthetic method
    PrattleChatEvent(IChatComponent x0, String x1, String x2, NamelessClass1290931309 x3) {
        this(x0, x1, x2);
    }

    // $FF: synthetic class
    static class NamelessClass1290931309 {
    }

    public static class ServerSendChatEvent extends PrattleChatEvent {

        public final Map flags;


        public ServerSendChatEvent(IChatComponent msg, String sender, String target, Map flags) {
            super(msg, sender, target, (NamelessClass1290931309) null);
            this.flags = flags;
        }
    }

    public static class ClientRecvChatEvent extends PrattleChatEvent {

        public ClientRecvChatEvent(IChatComponent msg, String sender, String target) {
            super(msg, sender, target, (NamelessClass1290931309) null);
        }
    }

    public static class ServerRecvChatEvent extends PrattleChatEvent {

        public final Map flags;


        public ServerRecvChatEvent(IChatComponent msg, String sender, String target, Map flags) {
            super(msg, sender, target, (NamelessClass1290931309) null);
            this.flags = flags;
        }
    }

    public static class ClientSendChatEvent extends PrattleChatEvent {

        public ClientSendChatEvent(IChatComponent msg, String sender, String target) {
            super(msg, sender, target, (NamelessClass1290931309) null);
        }
    }
}
