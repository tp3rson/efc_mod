package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatText;
import com.existence.mod.escapeFromChernakov.chatModule.server.Channel;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiNewChat;

public class MsgChannelEvent implements IMessage {

    private String channel;
    private String username;
    private Channel.ChannelEvent event;

    public MsgChannelEvent() {
    }

    public MsgChannelEvent(String channel, String username, Channel.ChannelEvent event) {
        this.channel = channel;
        this.username = username;
        this.event = event;
    }

    public void fromBytes(ByteBuf buf) {
        this.channel = ByteBufUtils.readUTF8String((ByteBuf) buf);
        this.username = ByteBufUtils.readUTF8String((ByteBuf) buf);
        this.event = Channel.ChannelEvent.values()[buf.readInt()];
    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String((ByteBuf) buf, (String) this.channel);
        ByteBufUtils.writeUTF8String((ByteBuf) buf, (String) this.username);
        buf.writeInt(this.event.ordinal());
    }

    public static class Handler implements IMessageHandler<MsgChannelEvent, IMessage> {
        public IMessage onMessage(MsgChannelEvent message, MessageContext ctx) {
            GuiNewChat gui = Minecraft.getMinecraft().ingameGUI.getChatGUI();
            if (gui instanceof GuiChatText) {
                switch (message.event) {
                    case JOIN: {
                        ((GuiChatText) gui).softAddChannel(message.channel);
                        ((GuiChatText) gui).switchDisplayChannel(message.channel);
                        break;
                    }
                    case PART: {
                        if (((GuiChatText) gui).getChannelDisplay().equals(message.channel)) {
                            ((GuiChatText) gui).switchDisplayChannel(Talkative.globalChat);
                            ((GuiChatText) gui).switchReceivingChannel(Talkative.globalChat);
                        }
                        ((GuiChatText) gui).rmChannel(message.channel);
                        break;
                    }
                }
            }
            return null;
        }
    }
}
