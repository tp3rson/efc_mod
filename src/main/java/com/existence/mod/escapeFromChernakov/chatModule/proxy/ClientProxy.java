package com.existence.mod.escapeFromChernakov.chatModule.proxy;

import com.existence.mod.escapeFromChernakov.chatModule.client.KeyBindings;
import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatText;
import com.existence.mod.escapeFromChernakov.chatModule.event.ClientEventHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraftforge.common.MinecraftForge;

import java.util.Random;

public class ClientProxy extends ServerProxy {

    static GuiNewChat oldChat;


    public void registerHandlers() {
        super.registerHandlers();
        FMLCommonHandler.instance().bus().register(new ClientEventHandler());
        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
    }

    public void registerKeyBindings() {
        KeyBindings.init();
    }

    public void injectGuiChatText() {
        Minecraft mc = Minecraft.getMinecraft();
        oldChat = mc.ingameGUI.persistantChatGUI;
        mc.ingameGUI.persistantChatGUI = new GuiChatText(mc);
    }

    public void playSound(String name) {
        Minecraft mc = Minecraft.getMinecraft();
        new Random();
        mc.theWorld.playSound(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ, name, 1.0F, 1.0F, false);
    }

    public Object getOldGlobalChan() {
        return oldChat;
    }
}
