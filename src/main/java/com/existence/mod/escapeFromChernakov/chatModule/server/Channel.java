package com.existence.mod.escapeFromChernakov.chatModule.server;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChannelEvent;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChatWrapperServer;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgPlayerList;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.IChatComponent;

import java.util.*;

public class Channel {

    protected Set users = new HashSet();
    private Set access = new HashSet();
    private Set ops = new HashSet();
    private String owner = "";
    private Map status = new HashMap();
    protected final String name;


    public Channel(String name) {
        this.name = name;
        ChanStat[] arr$ = ChanStat.values();
        int len$ = arr$.length;

        for (int i$ = 0; i$ < len$; ++i$) {
            ChanStat s = arr$[i$];
            this.status.put(s, Boolean.valueOf(false));
        }

    }

    public String getName() {
        return this.name;
    }

    public void setOwner(String name) {
        this.owner = name;
    }

    public String getOwner() {
        return this.owner;
    }

    public Set getOps() {
        return this.ops;
    }

    public void addOp(String name) {
        this.ops.add(name);
    }

    public void rmOp(String name) {
        this.ops.remove(name);
    }

    public boolean isOp(String name) {
        return this.getOwner().equals(name) || this.getOps().contains(name) || name.equals("Server") || name.equals("Rcon");
    }

    public boolean isServer(String name) {
        return name.equals("Server") || name.equals("Rcon");
    }

    public void dispatchUserList() {
        if (this.get(ChanStat.GLOBAL)) {
            HashSet i$ = new HashSet();
            Iterator p = this.getUsers().iterator();

            while (p.hasNext()) {
                EntityPlayer p1 = (EntityPlayer) p.next();
                i$.add(p1.getDisplayName());
            }

            p = this.users.iterator();

            while (p.hasNext()) {
                EntityPlayerMP p3 = (EntityPlayerMP) p.next();
                NetworkHelper.sendTo(new MsgPlayerList(this.getName(), i$), p3);
            }
        } else {
            Iterator i$1 = this.users.iterator();

            while (i$1.hasNext()) {
                EntityPlayerMP p2 = (EntityPlayerMP) i$1.next();
                NetworkHelper.sendTo(new MsgPlayerList(this.getName(), new HashSet()), p2);
            }
        }

    }

    public ReturnStatus addPlayer(EntityPlayerMP player) {
        if (this.getUsers().contains(player)) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You are already in channel %s.", new Object[]{this.getName()}));
        } else if (!this.get(ChanStat.JOIN) && !this.access.contains(player.getGameProfile().getName()) && !this.isOp(player.getGameProfile().getName())) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You cannot join channel %s. It is marked as INVITE only.", new Object[]{this.getName()}));
        } else {
            if (this.get(ChanStat.GLOBAL)) {
                Iterator i$ = this.users.iterator();

                while (i$.hasNext()) {
                    EntityPlayerMP p = (EntityPlayerMP) i$.next();
                    NetworkHelper.sendTo(new MsgChatWrapperServer(String.format("* %s joined the channel *", new Object[]{player.getDisplayName()}), this.getName(), this.getName()), p);
                }
            }

            this.users.add(player);
            this.dispatchUserList();
            if (this.owner.equals("")) {
                this.owner = player.getGameProfile().getName();
            }

            NetworkHelper.sendTo(new MsgChannelEvent(this.name, player.getDisplayName(), ChannelEvent.JOIN), player);
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Joined channel %s.", new Object[]{this.getName()}));
        }
    }

    public void rmAllPlayers() {
        Iterator i$ = this.users.iterator();

        while (i$.hasNext()) {
            EntityPlayerMP player = (EntityPlayerMP) i$.next();
            this.rmPlayer(player, true);
        }

    }

    public ReturnStatus rmPlayer(EntityPlayerMP player, boolean forced) {
        if (!this.getUsers().contains(player)) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You are not in channel %s.", new Object[]{this.getName()}));
        } else if ((!this.get(ChanStat.PART) || this.get(ChanStat.FORCED)) && !forced) {
            Talkative.log.info("Player " + player.getDisplayName() + " tried to leave a locked channel.");
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You cannot leave channel %s. It is marked as FORCED.", new Object[]{this.getName()}));
        } else {
            this.users.remove(player);
            if (this.get(ChanStat.GLOBAL)) {
                Iterator i$ = this.users.iterator();

                while (i$.hasNext()) {
                    EntityPlayerMP p = (EntityPlayerMP) i$.next();
                    NetworkHelper.sendTo(new MsgChatWrapperServer(String.format("* %s left the channel *", new Object[]{player.getDisplayName()}), this.getName(), this.getName()), p);
                }
            }

            this.dispatchUserList();
            NetworkHelper.sendTo(new MsgChannelEvent(this.name, player.getDisplayName(), ChannelEvent.PART), player);
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Left channel %s.", new Object[]{this.getName()}));
        }
    }

    public ReturnStatus invitePlayer(ICommandSender sender, EntityPlayerMP target) {
        if (!this.isOp(sender.getCommandSenderName())) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You don\'t have the rights to do that in channel %s.", new Object[]{this.getName()}));
        } else {
            this.access.add(target.getGameProfile().getName());
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.OK, String.format("You have been invited to %s by %s.", new Object[]{sender.getCommandSenderName(), this.getName()})), target);
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Player %s invited to %s.", new Object[]{target.getDisplayName(), this.getName()}));
        }
    }

    public ReturnStatus revokePlayer(ICommandSender sender, EntityPlayerMP target) {
        if (!this.isOp(sender.getCommandSenderName())) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You don\'t have the rights to do that in channel %s.", new Object[]{this.getName()}));
        } else if (!this.access.contains(target.getGameProfile().getName())) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Player %s is not in the invite list for channel %s.", new Object[]{target.getGameProfile().getName(), this.getName()}));
        } else {
            this.access.remove(target.getGameProfile().getName());
            this.rmPlayer(target, true);
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Player %s revoked from %s.", new Object[]{target.getDisplayName(), this.getName()}));
        }
    }

    public ReturnStatus sendMsg(IChatComponent msg, ICommandSender user) {
        if (this.isServer(user.getCommandSenderName())) {
            Iterator userp1 = this.getUsers().iterator();

            while (userp1.hasNext()) {
                EntityPlayerMP i$1 = (EntityPlayerMP) userp1.next();
                NetworkHelper.sendTo(new MsgChatWrapperServer(msg, this.getName(), user.getCommandSenderName()), i$1);
            }

            return new ReturnStatus(ReturnStatus.Status.OK);
        } else if (!(user instanceof EntityPlayerMP)) {
            return new ReturnStatus(ReturnStatus.Status.CRITICAL, "User isn\'t the server or a player, who are you???");
        } else {
            EntityPlayerMP userp = (EntityPlayerMP) user;
            if (!this.users.contains(userp)) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You are not in channel %s.", new Object[]{this.getName()}));
            } else {
                Iterator i$;
                EntityPlayerMP player;
                if (!this.get(ChanStat.GLOBAL)) {
                    i$ = this.getUsers().iterator();

                    while (i$.hasNext()) {
                        player = (EntityPlayerMP) i$.next();
                        if (userp.getDistanceToEntity(player) < (float) Talkative.localChatRange) {
                            NetworkHelper.sendTo(new MsgChatWrapperServer(msg, this.getName(), user.getCommandSenderName()), player);
                        }
                    }

                    return new ReturnStatus(ReturnStatus.Status.OK);
                } else {
                    i$ = this.getUsers().iterator();

                    while (i$.hasNext()) {
                        player = (EntityPlayerMP) i$.next();
                        NetworkHelper.sendTo(new MsgChatWrapperServer(msg, this.getName(), user.getCommandSenderName()), player);
                    }

                    return new ReturnStatus(ReturnStatus.Status.OK);
                }
            }
        }
    }

    public boolean isEmpty() {
        return this.users.size() == 0;
    }

    public Channel set(ChanStat s, boolean flag) {
        this.status.put(s, Boolean.valueOf(flag));
        return this;
    }

    public boolean get(ChanStat s) {
        return ((Boolean) this.status.get(s)).booleanValue();
    }

    public Map getFlagsAsMap() {
        HashMap retmap = new HashMap();
        Iterator i$ = this.status.keySet().iterator();

        while (i$.hasNext()) {
            ChanStat s = (ChanStat) i$.next();
            retmap.put(s.toString(), this.status.get(s));
        }

        return retmap;
    }

    public Set getUsers() {
        return this.users;
    }

    public Channel setDefaultAccess() {
        this.set(ChanStat.JOIN, true).set(ChanStat.PART, true).set(ChanStat.MODE, true).set(ChanStat.GLOBAL, true);
        return this;
    }

    public Channel readFromNBT(NBTTagCompound tag) {
        NBTTagCompound flagTag = tag.getCompoundTag("flags");
        Iterator accessTag = flagTag.func_150296_c().iterator();

        while (accessTag.hasNext()) {
            Object opsTag = accessTag.next();

            try {
                ChanStat i = ChanStat.valueOf((String) opsTag);
                this.set(i, flagTag.getBoolean((String) opsTag));
            } catch (Exception var6) {
                Talkative.log.error("Unknown flag found while loading channel " + this.getName() + " data : " + opsTag + ", skipping.");
            }
        }

        NBTTagList var7 = tag.getTagList("invites", 9);

        for (int var8 = 0; var8 < var7.tagCount(); ++var8) {
            this.access.add(var7.getStringTagAt(var8));
        }

        NBTTagList var10 = tag.getTagList("ops", 9);

        for (int var9 = 0; var9 < var10.tagCount(); ++var9) {
            this.ops.add(var10.getStringTagAt(var9));
        }

        return this;
    }

    public void writeToNBT(NBTTagCompound tag) {
        tag.setString("name", this.getName());
        tag.setString("owner", this.owner);
        NBTTagCompound flagTag = new NBTTagCompound();
        ChanStat[] accessTag = ChanStat.values();
        int opsTag = accessTag.length;

        for (int i$ = 0; i$ < opsTag; ++i$) {
            ChanStat s = accessTag[i$];
            flagTag.setBoolean(s.toString(), this.get(s));
        }

        tag.setTag("flags", flagTag);
        NBTTagList var7 = new NBTTagList();
        Iterator var8 = this.access.iterator();

        while (var8.hasNext()) {
            String var11 = (String) var8.next();
            var7.appendTag(new NBTTagString(var11));
        }

        tag.setTag("invites", var7);
        NBTTagList var9 = new NBTTagList();
        Iterator var10 = this.ops.iterator();

        while (var10.hasNext()) {
            String var12 = (String) var10.next();
            var9.appendTag(new NBTTagString(var12));
        }

        tag.setTag("ops", var9);
    }

    public static enum ChannelEvent {

        JOIN("JOIN", 0),
        PART("PART", 1),
        INVITE("INVITE", 2);
        // $FF: synthetic field
        private static final ChannelEvent[] $VALUES = new ChannelEvent[]{JOIN, PART, INVITE};


        private ChannelEvent(String var1, int var2) {
        }

    }

    public static enum ChanStat {

        JOIN("JOIN", 0, false, "If false, the chan becomes invite only. Zone chats are JOIN but you can\'t invite anyone in them."),
        PART("PART", 1, false, "If false, players can\'t leave the channel, or get kicked out of it. Good for zones, default chats, etc."),
        MODE("MODE", 2, true, "Control if channel parameters can be changed once set. [ADMIN ONLY]"),
        GLOBAL("GLOBAL", 3, false, "Global channels will use the standard user list, while local will also check for distance."),
        PERMANENT("PERMANENT", 4, true, "Permanent channels will not be deleted when empty (and so keep all configs). [ADMIN ONLY]"),
        FORCED("FORCED", 5, true, "Forced channels will be pushed to users on login. [ADMIN ONLY]");
        private String description;
        private boolean isOP_;
        // $FF: synthetic field
        private static final ChanStat[] $VALUES = new ChanStat[]{JOIN, PART, MODE, GLOBAL, PERMANENT, FORCED};


        private ChanStat(String var1, int var2, boolean isOP_, String description) {
            this.description = description;
            this.isOP_ = isOP_;
        }

        public String getDescription() {
            return this.description;
        }

        public boolean isOP() {
            return this.isOP_;
        }

    }
}
