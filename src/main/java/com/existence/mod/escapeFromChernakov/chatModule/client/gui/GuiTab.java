package com.existence.mod.escapeFromChernakov.chatModule.client.gui;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChatWrapperClient;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import org.lwjgl.opengl.GL11;

public class GuiTab extends GuiButton {

    private boolean selected;


    public GuiTab(int index, int x, int y, String label) {
        this(index, x, y, 200, 20, label);
    }

    public GuiTab(int index, int x, int y, int w, int h, String label) {
        super(index, x, y, w, h, label);
        this.selected = false;
    }

    public void setSelected(boolean flag) {
        this.selected = flag;
    }

    public boolean getSelected() {
        return this.selected;
    }

    public void mouseEvent(Minecraft mc, int xpos, int ypos, int which) {
        if (which == 2) {
            NetworkHelper.sendToServer(new MsgChatWrapperClient("/part " + super.displayString, ""));
        }

    }

    public void drawButton(Minecraft mc, int xpos, int ypos) {
        if (super.visible) {
            String outStr = super.displayString;
            if (super.displayString.equals(Talkative.globalChat)) {
                outStr = Talkative.globalChatAlias;
            }

            if (super.displayString.equals(Talkative.localChat)) {
                outStr = Talkative.localChatAlias;
            }

            GuiChatText gui = (GuiChatText) mc.ingameGUI.getChatGUI();
            if (gui.getUnread(super.displayString) > 0) {
                outStr = outStr + " [" + gui.getUnread(super.displayString) + "]";
            }

            FontRenderer fontrenderer = mc.fontRenderer;
            mc.getTextureManager().bindTexture(GuiButton.buttonTextures);
            GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            super.field_146123_n = xpos >= super.xPosition && ypos >= super.yPosition && xpos < super.xPosition + super.width && ypos < super.yPosition + super.height;
            int k = this.getHoverState(super.field_146123_n);
            drawRect(super.xPosition, super.yPosition, super.xPosition + super.width, super.yPosition + super.height, Integer.MIN_VALUE);
            if (this.getSelected()) {
                drawRect(super.xPosition, super.yPosition, super.xPosition + super.width, super.yPosition + 1, Integer.MAX_VALUE);
                drawRect(super.xPosition, super.yPosition, super.xPosition + 1, super.yPosition + super.height, Integer.MAX_VALUE);
                drawRect(super.xPosition + super.width, super.yPosition, super.xPosition + super.width - 1, super.yPosition + super.height, Integer.MAX_VALUE);
            } else {
                drawRect(super.xPosition, super.yPosition + super.height - 1, super.xPosition + super.width, super.yPosition + super.height, Integer.MAX_VALUE);
            }

            this.mouseDragged(mc, xpos, ypos);
            int l = 14737632;
            if (super.packedFGColour != 0) {
                l = super.packedFGColour;
            } else if (!super.enabled) {
                l = 10526880;
            } else if (super.field_146123_n) {
                l = 16777120;
            } else if (!this.getSelected()) {
                l = 8421504;
            }

            this.drawCenteredString(fontrenderer, outStr, super.xPosition + super.width / 2, super.yPosition + (super.height - 8) / 2, l);
        }

    }
}
