package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.api.PrattleChatEvent;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.common.MinecraftForge;

public class NetworkHelper {

    public static SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel("Prattle");


    public static void registerMessages() {
        network.registerMessage(MsgChatWrapperClient.Handler.class, MsgChatWrapperClient.class, 0, Side.SERVER);
        network.registerMessage(MsgChatWrapperServer.Handler.class, MsgChatWrapperServer.class, 1, Side.CLIENT);
        network.registerMessage(MsgPostlogInit.Handler.class, MsgPostlogInit.class, 2, Side.CLIENT);
        network.registerMessage(MsgChannelEvent.Handler.class, MsgChannelEvent.class, 3, Side.CLIENT);
        network.registerMessage(MsgPlayerList.Handler.class, MsgPlayerList.class, 4, Side.CLIENT);
        network.registerMessage(MsgSystemMessage.Handler.class, MsgSystemMessage.class, 5, Side.CLIENT);
        network.registerMessage(MsgChannelSwitch.Handler.class, MsgChannelSwitch.class, 6, Side.SERVER);
    }

    public static void sendTo(IMessage msg, EntityPlayerMP player) {
        if (msg instanceof MsgChatWrapperServer && ((MsgChatWrapperServer) msg).getChannel().startsWith("#")) {
            PrattleChatEvent.ServerSendChatEvent event = new PrattleChatEvent.ServerSendChatEvent(((MsgChatWrapperServer) msg).getMsg(), ((MsgChatWrapperServer) msg).getSender(), ((MsgChatWrapperServer) msg).getChannel(), ChannelHandler.INSTANCE.getChannel(((MsgChatWrapperServer) msg).getChannel()).getFlagsAsMap());
            if (MinecraftForge.EVENT_BUS.post(event)) {
                return;
            }

            ((MsgChatWrapperServer) msg).setMsg(event.displayMsg);
            network.sendTo(msg, player);
        } else {
            network.sendTo(msg, player);
        }

    }

    @SideOnly(Side.CLIENT)
    public static void sendToServer(IMessage msg) {
        if (msg instanceof MsgChatWrapperClient && ((MsgChatWrapperClient) msg).getTarget().startsWith("#")) {
            PrattleChatEvent.ClientSendChatEvent event = new PrattleChatEvent.ClientSendChatEvent(((MsgChatWrapperClient) msg).getMsg(), Minecraft.getMinecraft().thePlayer.getCommandSenderName(), ((MsgChatWrapperClient) msg).getTarget());
            if (MinecraftForge.EVENT_BUS.post(event)) {
                return;
            }

            ((MsgChatWrapperClient) msg).setMsg(event.displayMsg);
            network.sendToServer(msg);
        } else {
            network.sendToServer(msg);
        }

    }

    public static void sendChatMessage(IChatComponent msg, String sender, ICommandSender target, String targetChannel) {
        if (targetChannel == null) {
            targetChannel = Talkative.globalChat;
        }

        if (sender == null) {
            sender = "";
        }

        if (!msg.equals("")) {
            if (target instanceof EntityPlayerMP) {
                sendTo(new MsgChatWrapperServer(msg, targetChannel, sender), (EntityPlayerMP) target);
            } else {
                target.addChatMessage(msg);
            }
        }

    }

    public static void sendSystemMessage(ReturnStatus stat, ICommandSender sender) {
        if (sender instanceof EntityPlayerMP && stat.hasMessage()) {
            network.sendTo(new MsgSystemMessage(stat.msg, stat.status), (EntityPlayerMP) sender);
        } else if (!(sender instanceof EntityPlayerMP) && stat.hasMessage()) {
            sender.addChatMessage(new ChatComponentText(stat.msg));
        }

    }

}
