package com.existence.mod.escapeFromChernakov.chatModule.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;

public class CommandList extends CommandBase {

    public String getCommandName() {
        return "list";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/list";
    }

    public void processCommand(ICommandSender sender, String[] data) {
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
