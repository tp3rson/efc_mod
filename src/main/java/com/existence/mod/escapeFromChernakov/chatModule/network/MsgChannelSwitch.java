package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;

public class MsgChannelSwitch implements IMessage {

    private String channel;


    public MsgChannelSwitch() {
    }

    public MsgChannelSwitch(String channel) {
        this.channel = channel;
    }

    public void fromBytes(ByteBuf buf) {
        this.channel = ByteBufUtils.readUTF8String(buf);
    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.channel);
    }

    public static class Handler implements IMessageHandler<MsgChannelSwitch, IMessage> {
        public IMessage onMessage(MsgChannelSwitch message, MessageContext ctx) {
            ChannelHandler.INSTANCE.setOpenTab(ctx.getServerHandler().playerEntity, message.channel);
            return null;
        }
    }
}
