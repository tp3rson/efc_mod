package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;

public class MsgPostlogInit implements IMessage {

    public void fromBytes(ByteBuf buf) {
        Talkative.maxChatChars = buf.readInt();
        Talkative.globalChat = ByteBufUtils.readUTF8String((ByteBuf) buf);
        Talkative.localChat = ByteBufUtils.readUTF8String((ByteBuf) buf);
    }

    public void toBytes(ByteBuf buf) {
        buf.writeInt(Talkative.maxChatChars);
        ByteBufUtils.writeUTF8String((ByteBuf) buf, (String) Talkative.globalChat);
        ByteBufUtils.writeUTF8String((ByteBuf) buf, (String) Talkative.localChat);
    }

    public static class Handler implements IMessageHandler<MsgPostlogInit, IMessage> {
        public IMessage onMessage(MsgPostlogInit message, MessageContext ctx) {
            Talkative.proxy.injectGuiChatText();
            return null;
        }
    }
}
