package com.existence.mod.escapeFromChernakov.chatModule.commands;

import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;

public class CommandInvite extends CommandBase {

    public String getCommandName() {
        return "invite";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/invite <channel> <player>";
    }

    public void processCommand(ICommandSender sender, String[] data) {
        if (data.length >= 2 && data[0].startsWith("#")) {
            ReturnStatus stat = ChannelHandler.INSTANCE.inviteToChannel(data[0], sender, data[1]);
            NetworkHelper.sendSystemMessage(stat, sender);
        } else {
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, this.getCommandUsage(sender)), sender);
        }
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
