package com.existence.mod.escapeFromChernakov.chatModule;

import java.lang.reflect.Field;

public class ReflectionHelper {

    public static void assignField(String name_class, String name_clear, String name_srg, Object target_obj, Object target) {
        String fieldName = name_clear;

        try {
            Class.forName(name_class).getDeclaredField(fieldName);
        } catch (Exception var13) {
            fieldName = name_srg;
        }

        try {
            Field targetField = Class.forName(name_class).getDeclaredField(fieldName);
            targetField.setAccessible(true);
            Field e = Field.class.getDeclaredField("modifiers");
            e.setAccessible(true);
            e.setInt(targetField, targetField.getModifiers() & -17);
            targetField.set(target_obj, target);
        } catch (NoSuchFieldException var8) {
            var8.printStackTrace();
        } catch (SecurityException var9) {
            var9.printStackTrace();
        } catch (ClassNotFoundException var10) {
            var10.printStackTrace();
        } catch (IllegalArgumentException var11) {
            var11.printStackTrace();
        } catch (IllegalAccessException var12) {
            var12.printStackTrace();
        }

    }
}
