package com.existence.mod.escapeFromChernakov.chatModule.api;

import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PrattleAPI {

    private static boolean isPrattleAvailable = false;
    private static Logger log = LogManager.getLogger("PrattleAPI");


    public static String getDisplayChannel(EntityPlayerMP player) {
        if (!isPrattleAvailable) {
            log.error("Trying to make an API call while Prattle is not installed !! getDisplayChannel()");
            return null;
        } else {
            return ChannelHandler.INSTANCE.getOpenTab(player);
        }
    }

    public static void sendMessage(ICommandSender target, IChatComponent msg, String targetChannel, String sender) {
        if (!isPrattleAvailable) {
            log.error("Trying to make an API call while Prattle is not installed !! sendMessage()");
        } else {
            NetworkHelper.sendChatMessage(msg, sender, target, targetChannel);
        }
    }

    public static void sendMessage(ICommandSender target, String msg, String targetChannel) {
        sendMessage(target, (IChatComponent) (new ChatComponentText(msg)), targetChannel, (String) null);
    }

    public static void sendMessage(ICommandSender target, IChatComponent msg, String targetChannel) {
        sendMessage(target, msg, targetChannel, (String) null);
    }

    public static void sendMessage(ICommandSender target, String msg, String targetChannel, String sender) {
        sendMessage(target, (IChatComponent) (new ChatComponentText(msg)), targetChannel, sender);
    }

    static {
        try {
            Class.forName("com.existence.mod.escapeFromChernakov.chatModule.Talkative");
            isPrattleAvailable = true;
        } catch (ClassNotFoundException var1) {
            isPrattleAvailable = false;
        }

    }
}
