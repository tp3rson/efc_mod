package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatText;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MsgPlayerList implements IMessage {

    String channel;
    Set players = new HashSet();


    public MsgPlayerList() {
    }

    public MsgPlayerList(String channel, Set players) {
        this.channel = channel;
        this.players = players;
    }

    public void fromBytes(ByteBuf buf) {
        this.channel = ByteBufUtils.readUTF8String(buf);
        int size = buf.readInt();

        for (int i = 0; i < size; ++i) {
            this.players.add(ByteBufUtils.readUTF8String(buf));
        }

    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, this.channel);
        buf.writeInt(this.players.size());
        Iterator i$ = this.players.iterator();

        while (i$.hasNext()) {
            String s = (String) i$.next();
            ByteBufUtils.writeUTF8String(buf, s);
        }

    }

    public static class Handler implements IMessageHandler<MsgPlayerList, IMessage> {
        public IMessage onMessage(MsgPlayerList message, MessageContext ctx) {
            ((GuiChatText) Minecraft.getMinecraft().ingameGUI.getChatGUI()).setUserList(message.channel, message.players);
            return null;
        }
    }
}
