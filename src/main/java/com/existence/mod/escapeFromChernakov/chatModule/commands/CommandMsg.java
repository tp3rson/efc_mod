package com.existence.mod.escapeFromChernakov.chatModule.commands;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import com.existence.mod.escapeFromChernakov.chatModule.utils.StringHelper;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.command.server.CommandMessage;
import net.minecraft.entity.player.EntityPlayerMP;

import java.util.Arrays;
import java.util.List;

public class CommandMsg extends CommandMessage {

    public List getCommandAliases() {
        return Arrays.asList(new String[]{"w", "msg", "pm"});
    }

    public void processCommand(ICommandSender sender, String[] data) {
        if (data.length < 2) {
            throw new WrongUsageException("commands.message.usage", new Object[0]);
        } else {
            String s = "";

            for (int stat = 1; stat < data.length; ++stat) {
                s = s + data[stat];
                if (stat != data.length - 1) {
                    s = s + " ";
                }
            }

            data[0] = StringHelper.getTextWithoutFormattingCodes(data[0]);
            ReturnStatus var6 = ChannelHandler.INSTANCE.sendMessageToChannel(data[0], s, sender);
            if (var6.status == ReturnStatus.Status.OK && data[0].startsWith("#")) {
                String outString = data[0];
                if (outString.equals(Talkative.globalChat)) {
                    outString = Talkative.globalChatAlias;
                }

                if (outString.equals(Talkative.localChat)) {
                    outString = Talkative.localChatAlias;
                }

                Talkative.log.info("[" + outString + "]<" + sender.getCommandSenderName() + "> " + s);
            }

            NetworkHelper.sendSystemMessage(var6, (EntityPlayerMP) sender);
        }
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
