package com.existence.mod.escapeFromChernakov.chatModule.event;

import com.existence.mod.escapeFromChernakov.chatModule.client.KeyBindings;
import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatInput;
import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatText;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer.EnumChatVisibility;

public class ClientEventHandler {

    private Minecraft mc = Minecraft.getMinecraft();


    @SubscribeEvent
    public void onKeyInput(KeyInputEvent event) {
        boolean chatVisible = this.mc.gameSettings.chatVisibility != EnumChatVisibility.HIDDEN;
        if (KeyBindings.chatOpen.isPressed() && chatVisible) {
            this.mc.displayGuiScreen(new GuiChatInput());
        }

        if (KeyBindings.chatCommand.isPressed() && chatVisible && this.mc.currentScreen == null) {
            this.mc.displayGuiScreen(new GuiChatInput("/"));
        }

        if (KeyBindings.chatReply.isPressed() && chatVisible) {
            GuiChatText gui = (GuiChatText) this.mc.ingameGUI.getChatGUI();
            gui.switchDisplayChannel(gui.getChannelLast());
            this.mc.displayGuiScreen(new GuiChatInput());
        }

    }
}
