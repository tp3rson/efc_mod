package com.existence.mod.escapeFromChernakov.chatModule.network;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.client.gui.GuiChatText;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.network.play.server.S02PacketChat;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.IChatComponent.Serializer;

public class MsgSystemMessage implements IMessage {

    private ReturnStatus.Status status;
    private IChatComponent msg;


    public MsgSystemMessage() {
    }

    public MsgSystemMessage(IChatComponent msg, ReturnStatus.Status status) {
        this.msg = msg;
        this.status = status;
    }

    public MsgSystemMessage(String msg, ReturnStatus.Status status) {
        this.msg = new ChatComponentText(msg);
        this.status = status;
    }

    public void fromBytes(ByteBuf buf) {
        this.msg = Serializer.func_150699_a(ByteBufUtils.readUTF8String(buf));
        this.status = ReturnStatus.Status.values()[buf.readInt()];
    }

    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeUTF8String(buf, Serializer.func_150696_a(this.msg));
        buf.writeInt(this.status.ordinal());
    }

    public static class Handler implements IMessageHandler<MsgSystemMessage, IMessage> {
        public IMessage onMessage(MsgSystemMessage message, MessageContext ctx) {
            GuiChatText guiChat;
            if (message.status != ReturnStatus.Status.OK) {
                if (message.status == ReturnStatus.Status.HELP) {
                    message.msg.getChatStyle().setColor(EnumChatFormatting.GRAY);
                    message.msg.getChatStyle().setItalic(Boolean.valueOf(true));
                } else if (message.status == ReturnStatus.Status.ERROR) {
                    message.msg.getChatStyle().setColor(EnumChatFormatting.RED);
                } else if (message.status == ReturnStatus.Status.CRITICAL) {
                    message.msg.getChatStyle().setColor(EnumChatFormatting.RED);
                    message.msg.getChatStyle().setBold(Boolean.valueOf(true));
                } else {
                    message.msg.getChatStyle().setColor(EnumChatFormatting.BLUE);
                }
            }
            if (!(guiChat = (GuiChatText) Minecraft.getMinecraft().ingameGUI.getChatGUI()).getChannelDisplay().equals(Talkative.globalChat)) {
                guiChat.switchReceivingChannel(guiChat.getChannelDisplay());
                ctx.getClientHandler().handleChat(new S02PacketChat(message.msg));
            }
            guiChat.switchReceivingChannel(Talkative.globalChat);
            ctx.getClientHandler().handleChat(new S02PacketChat(message.msg));
            return null;
        }
    }
}
