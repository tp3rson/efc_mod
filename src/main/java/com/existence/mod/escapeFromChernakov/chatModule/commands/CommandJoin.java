package com.existence.mod.escapeFromChernakov.chatModule.commands;

import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.server.ChannelHandler;
import com.existence.mod.escapeFromChernakov.chatModule.server.ReturnStatus;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;

public class CommandJoin extends CommandBase {

    public String getCommandName() {
        return "join";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/join <channel>. Channels start with a #";
    }

    public void processCommand(ICommandSender sender, String[] data) {
        if (data.length >= 1 && data[0].startsWith("#")) {
            if (sender instanceof EntityPlayerMP) {
                ReturnStatus stat = ChannelHandler.INSTANCE.joinChannel(data[0], (EntityPlayerMP) sender);
                NetworkHelper.sendSystemMessage(stat, sender);
            } else {
                NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.ERROR, "Only players can join channels."), sender);
            }

        } else {
            NetworkHelper.sendSystemMessage(new ReturnStatus(ReturnStatus.Status.HELP, this.getCommandUsage(sender)), sender);
        }
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
