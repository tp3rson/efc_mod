package com.existence.mod.escapeFromChernakov.chatModule;

import com.existence.mod.escapeFromChernakov.chatModule.commands.*;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import com.existence.mod.escapeFromChernakov.chatModule.proxy.ServerProxy;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.Metadata;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//@Mod(
//   modid = "Prattle",
//   name = "Prattle",
//   version = "0.0.9"
//)
public class Talkative {

    public static final String modid = "???";
    public static final String version = "0.0.9";
    @Instance("Prattle")
    public static Talkative instance;
    @Metadata("Prattle")
    public static ModMetadata meta;
    @SidedProxy(
            clientSide = "com.existence.mod.escapeFromChernakov.chatModule.proxy.ClientProxy",
            serverSide = "com.existence.mod.escapeFromChernakov.chatModule.proxy.ServerProxy"
    )
    public static ServerProxy proxy;
    public static Logger log = LogManager.getLogger("Prattle");
    public static Configuration config = null;
    public static int maxChatChars = 300;
    public static String globalChat = "#global";
    public static String localChat = "#local";
    public static String globalChatAlias = "#global";
    public static String localChatAlias = "#local";
    public static String[] eventWhitelist = new String[]{globalChat};
    public static int localChatRange = 60;
    public static boolean showNotices = true;
    public static boolean soundNotices = true;
    public static String soundName = "minecraft:mob.chicken.plop";
    public static boolean canPlayerCreate = false;
    public static boolean showTimestamp = true;
    public static boolean timestamp24h = true;


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        NetworkHelper.registerMessages();
        proxy.registerKeyBindings();
        config = new Configuration(event.getSuggestedConfigurationFile());

        try {
            config.load();
            maxChatChars = config.getInt("maxChatChars", "general", 300, 100, 1000, "Maximum number of chars per chat message. Vanilla default : 100");
            globalChatAlias = config.getString("globalChannel", "general", "#global", "Default name of the global channel (default channel corresponding to the Vanilla chat)");
            localChatAlias = config.getString("localChannel", "general", "#local", "Default name of the local channel (proximity channel used for local chatting).");
            localChatRange = config.getInt("localRange", "general", 60, 0, Integer.MAX_VALUE, "Range of the local channel, in blocks. A value of 0 will desactivate the local chat.");
            eventWhitelist = config.getStringList("eventWhitelist", "general", new String[]{globalChat}, "Whitelist of channels on which ChatEvents will be triggered. This is important because most IRC bridges, chat mods and such are using this event to trigger effects. You want to be carefull what you trigger it on, unless you want a lot of side effects.");
            showNotices = config.getBoolean("showNotices", "general", true, "Should we display the bottom left notices for new messages ?");
            canPlayerCreate = config.getBoolean("canPlayerCreateChannels", "general", false, "Can normal players create new channels ? Player created channels will be non persistant and can\'t be forced (Admin only can do that)");
            soundNotices = config.getBoolean("soundNotices", "general", true, "Should we play a short sound when receiving new messages ?");
            soundName = config.getString("soundName", "general", "minecraft:mob.chicken.plop", "If sound notices are on, this is the effect name that will be played.");
            showTimestamp = config.getBoolean("showTimestamp", "general", true, "Should the chat messages be timestamped ?");
            timestamp24h = config.getBoolean("timestamp24h", "general", true, "Format of the timestamp (true = 24h, false = am/pm).");
        } catch (Exception var6) {
            FMLLog.severe("Prattle has a problem loading it\'s configuration\r\n%s", new Object[]{var6});
            FMLLog.severe(var6.getMessage(), new Object[0]);
        } finally {
            if (config.hasChanged()) {
                config.save();
            }

        }

    }

    @EventHandler
    public void initialize(FMLInitializationEvent event) {
        proxy.registerHandlers();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    }

    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandJoin());
        event.registerServerCommand(new CommandPart());
        event.registerServerCommand(new CommandMsg());
        event.registerServerCommand(new CommandInvite());
        event.registerServerCommand(new CommandRevoke());
        event.registerServerCommand(new CommandChannel());
        event.registerServerCommand(new CommandChatZone());
        event.registerServerCommand(new CommandPrattleHelp());
    }

}
