package com.existence.mod.escapeFromChernakov.chatModule.proxy;

import com.existence.mod.escapeFromChernakov.chatModule.event.ServerEventHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;

public class ServerProxy {

    public void registerHandlers() {
        FMLCommonHandler.instance().bus().register(new ServerEventHandler());
        MinecraftForge.EVENT_BUS.register(new ServerEventHandler());
    }

    public void registerKeyBindings() {
    }

    public void injectGuiChatText() {
    }

    public void playSound(String name) {
    }

    public Object getOldGlobalChan() {
        return null;
    }
}
