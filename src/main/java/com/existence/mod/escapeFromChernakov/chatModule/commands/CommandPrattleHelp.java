package com.existence.mod.escapeFromChernakov.chatModule.commands;

import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class CommandPrattleHelp extends CommandBase {

    public String getCommandName() {
        return "prattle";
    }

    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/prattle : Display all commands related to Prattle";
    }

    public void processCommand(ICommandSender sender, String[] p_71515_2_) {
        sender.addChatMessage(new ChatComponentText("== PRATTLE HELP =="));
        sender.addChatMessage(new ChatComponentText("Available commands :"));
        sender.addChatMessage(new ChatComponentText((new CommandJoin()).getCommandUsage(sender)));
        sender.addChatMessage(new ChatComponentText((new CommandPart()).getCommandUsage(sender)));
        sender.addChatMessage(new ChatComponentText((new CommandInvite()).getCommandUsage(sender)));
        sender.addChatMessage(new ChatComponentText((new CommandRevoke()).getCommandUsage(sender)));
        sender.addChatMessage(new ChatComponentText((new CommandChannel()).getCommandUsage(sender)));
        sender.addChatMessage(new ChatComponentText((new CommandChatZone()).getCommandUsage(sender)));
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return true;
    }
}
