package com.existence.mod.escapeFromChernakov.chatModule.server;

import com.existence.mod.escapeFromChernakov.chatModule.Talkative;
import com.existence.mod.escapeFromChernakov.chatModule.api.PrattleChatEvent;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChannelEvent;
import com.existence.mod.escapeFromChernakov.chatModule.network.MsgChatWrapperServer;
import com.existence.mod.escapeFromChernakov.chatModule.network.NetworkHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.world.WorldSavedData;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.MinecraftForge;

import java.util.*;

public class ChannelHandler {

    public static ChannelHandler INSTANCE = new ChannelHandler();
    private Map channels = new LinkedHashMap();
    private Map zones = new HashMap();
    private Map temporaryZones = new HashMap();
    private Map openChannel = new HashMap();
    public ChannelSaveHandler channelSave = new ChannelSaveHandler();


    private ChannelHandler() {
        if (Talkative.localChatRange > 0) {
            this.createChannel(Talkative.localChat).set(Channel.ChanStat.FORCED, true).set(Channel.ChanStat.GLOBAL, false).set(Channel.ChanStat.JOIN, true).set(Channel.ChanStat.PERMANENT, true);
        }

        this.createChannel(Talkative.globalChat).set(Channel.ChanStat.FORCED, true).set(Channel.ChanStat.GLOBAL, true).set(Channel.ChanStat.JOIN, true).set(Channel.ChanStat.PERMANENT, true);
    }

    public Channel createChannel(String name_) {
        String name = name_.toLowerCase();
        if (!this.channels.containsKey(name)) {
            this.channels.put(name, new Channel(name));
        }

        this.channelSave.markDirty();
        return (Channel) this.channels.get(name);
    }

    public Channel addChanel(Channel chan) {
        String name = chan.getName().toLowerCase();
        if (chan instanceof ChannelZone) {
            this.zones.put(name, (ChannelZone) chan);
        }

        this.channels.put(name, chan);
        this.channelSave.markDirty();
        return chan;
    }

    public void rmChannel(Channel chan) {
        this.channels.remove(chan.getName());
        this.zones.remove(chan.getName());
        this.channelSave.markDirty();
        Talkative.log.info("Channel " + chan.getName() + " deleted.");
    }

    public Channel getChannel(String name) {
        return (Channel) this.channels.get(name.toLowerCase());
    }

    public Map getChannels() {
        return this.channels;
    }

    public Map getZones() {
        return this.zones;
    }

    public ReturnStatus startZone(String name, EntityPlayerMP player) {
        if (this.channels.containsKey(name)) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("A channel with name %s already exists.", new Object[]{name}));
        } else {
            this.temporaryZones.put(player, new ChannelZone(name, player.posX, player.posZ));
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Starting creating zone %s at coordinates [%.2f %.2f]", new Object[]{name, Double.valueOf(player.posX), Double.valueOf(player.posY)}));
        }
    }

    public ReturnStatus stopZone(String name, EntityPlayerMP player) {
        if (!this.temporaryZones.containsKey(player)) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, "You need to first start a zone before you can stop creating it !");
        } else {
            ChannelZone zone = (ChannelZone) this.temporaryZones.get(player);
            this.temporaryZones.remove(player);
            if (this.channels.containsKey(zone.getName())) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("A channel with name %s already exists.", new Object[]{zone.getName()}));
            } else {
                zone.addSecondPoint(player.posX, player.posZ);
                zone.setDefaultAccess();
                this.addChanel(zone);
                return new ReturnStatus(ReturnStatus.Status.OK, String.format("Zone %s created between [%.2f %.2f] and [%.2f %.2f]", new Object[]{zone.getName(), Double.valueOf(zone.xmin), Double.valueOf(zone.zmin), Double.valueOf(zone.xmax), Double.valueOf(zone.zmax)}));
            }
        }
    }

    public ReturnStatus removeZone(String name, EntityPlayerMP player) {
        if (!this.channels.containsKey(name) && !this.zones.containsKey(name)) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Zone %s doesn\'t exists.", new Object[]{name}));
        } else {
            ((ChannelZone) this.zones.get(name)).rmAllPlayers();
            this.channels.remove(name);
            this.zones.remove(name);
            this.temporaryZones.remove(name);
            this.channelSave.markDirty();
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Zone %s removed", new Object[]{name}));
        }
    }

    public ReturnStatus joinChannel(String name, EntityPlayerMP player) {
        if (name.startsWith("#")) {
            Channel chan;
            if (!this.channels.containsKey(name)) {
                if (!Talkative.canPlayerCreate && !player.mcServer.getConfigurationManager().func_152596_g(player.getGameProfile())) {
                    return new ReturnStatus(ReturnStatus.Status.ERROR, "Server settings prevent normal users to create new channels.");
                }

                chan = this.createChannel(name).setDefaultAccess();
            } else {
                chan = this.getChannel(name);
            }

            return chan.addPlayer(player);
        } else {
            NetworkHelper.sendTo(new MsgChannelEvent(name, player.getDisplayName(), Channel.ChannelEvent.JOIN), player);
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Joined channel %s.", new Object[]{name}));
        }
    }

    public void playerDisconnected(EntityPlayerMP player) {
        Iterator i$ = this.channels.keySet().iterator();

        while (i$.hasNext()) {
            String s = (String) i$.next();
            this.leaveChannel(s, player, true);
        }

        this.openChannel.remove(player);
    }

    public ReturnStatus leaveChannel(String name, EntityPlayerMP player) {
        return this.leaveChannel(name, player, false);
    }

    public ReturnStatus leaveChannel(String name, EntityPlayerMP player, boolean forced) {
        if (name.startsWith("#") && this.getChannel(name) != null) {
            Channel chan = this.getChannel(name);
            ReturnStatus stat = chan.rmPlayer(player, forced);
            if (!chan.get(Channel.ChanStat.PERMANENT) && chan.isEmpty()) {
                this.rmChannel(chan);
            }

            return stat;
        } else if (name.startsWith("#") && this.getChannel(name) == null) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Channel %s not found.", new Object[]{name}));
        } else {
            NetworkHelper.sendTo(new MsgChannelEvent(name, player.getDisplayName(), Channel.ChannelEvent.PART), player);
            return new ReturnStatus(ReturnStatus.Status.OK, String.format("Left channel %s.", new Object[]{name}));
        }
    }

    public ReturnStatus sendMessageToChannel(String name, String msg, ICommandSender user) {
        ChatComponentTranslation chatComponent = new ChatComponentTranslation("chat.type.text", new Object[]{user.func_145748_c_(), ForgeHooks.newChatWithLinks(msg)});
        if (name.startsWith("#") && this.getChannel(name) != null) {
            if (Arrays.asList(Talkative.eventWhitelist).contains(name) && user instanceof EntityPlayerMP) {
                chatComponent = ForgeHooks.onServerChatEvent(((EntityPlayerMP) user).playerNetServerHandler, msg, chatComponent);
                if (chatComponent == null) {
                    return new ReturnStatus(ReturnStatus.Status.OK);
                }
            }

            PrattleChatEvent.ServerRecvChatEvent target1 = new PrattleChatEvent.ServerRecvChatEvent(chatComponent, user.getCommandSenderName(), name, this.getChannel(name).getFlagsAsMap());
            return MinecraftForge.EVENT_BUS.post(target1) ? new ReturnStatus(ReturnStatus.Status.OK) : this.getChannel(name).sendMsg(target1.displayMsg, user);
        } else if (name.startsWith("#") && this.getChannel(name) == null) {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Channel %s not found.", new Object[]{name}));
        } else {
            EntityPlayerMP target = CommandBase.getPlayer(user, name);
            if (target == null) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Player %s not found.", new Object[]{name}));
            } else if (target == user) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You can\'t message yourself", new Object[]{name}));
            } else {
                if (user instanceof EntityPlayerMP) {
                    NetworkHelper.sendTo(new MsgChatWrapperServer(chatComponent, target.getDisplayName(), user.getCommandSenderName()), (EntityPlayerMP) user);
                    NetworkHelper.sendTo(new MsgChatWrapperServer(chatComponent, ((EntityPlayerMP) user).getDisplayName(), user.getCommandSenderName()), target);
                } else {
                    user.addChatMessage(new ChatComponentText("[" + target.getDisplayName() + "] " + chatComponent.getUnformattedTextForChat()));
                    NetworkHelper.sendTo(new MsgChatWrapperServer(chatComponent, user.getCommandSenderName(), user.getCommandSenderName()), target);
                }

                return new ReturnStatus(ReturnStatus.Status.OK);
            }
        }
    }

    public ReturnStatus changeMode(String name, Channel.ChanStat field, boolean flag, EntityPlayerMP user) {
        if (name.startsWith("#") && this.getChannel(name) != null) {
            if (!this.getChannel(name).get(Channel.ChanStat.MODE)) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("MODE is set to false for channel %s. You cannot change the flags for it.", new Object[]{name}));
            } else {
                this.getChannel(name).set(field, flag);
                this.channelSave.markDirty();
                return new ReturnStatus(ReturnStatus.Status.OK, String.format("Flag %s set to %s for channel %s", new Object[]{field, Boolean.valueOf(flag), name}));
            }
        } else {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Channel %s not found.", new Object[]{name}));
        }
    }

    public ReturnStatus inviteToChannel(String name, ICommandSender user, String targetname) {
        if (name.startsWith("#") && this.getChannel(name) != null) {
            EntityPlayerMP target = CommandBase.getPlayer(user, targetname);
            if (target == null) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Player %s not found.", new Object[]{targetname}));
            } else {
                this.channelSave.markDirty();
                return this.getChannel(name).invitePlayer(user, target);
            }
        } else {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Channel %s not found.", new Object[]{name}));
        }
    }

    public ReturnStatus revokeFromChannel(String name, ICommandSender user, String targetname) {
        if (name.startsWith("#") && this.getChannel(name) != null) {
            EntityPlayerMP target = CommandBase.getPlayer(user, targetname);
            if (target == null) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Player %s not found.", new Object[]{targetname}));
            } else if (target == user) {
                return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("You cannot revoke your own invitation.", new Object[0]));
            } else {
                this.channelSave.markDirty();
                Channel chan = this.getChannel(name);
                ReturnStatus stat = chan.revokePlayer(user, target);
                if (!chan.get(Channel.ChanStat.PERMANENT) && chan.isEmpty()) {
                    this.rmChannel(chan);
                }

                return stat;
            }
        } else {
            return new ReturnStatus(ReturnStatus.Status.ERROR, String.format("Channel %s not found.", new Object[]{name}));
        }
    }

    public void markDirty() {
        this.channelSave.markDirty();
    }

    public void setOpenTab(EntityPlayerMP player, String channel) {
        this.openChannel.put(player, channel);
    }

    public String getOpenTab(EntityPlayerMP player) {
        return (String) this.openChannel.get(player);
    }


    public static class ChannelSaveHandler extends WorldSavedData {

        public static String saveKey = "Prattle";


        public ChannelSaveHandler() {
            super(saveKey);
            this.markDirty();
        }

        public ChannelSaveHandler(String key) {
            super(key);
            this.markDirty();
        }

        public void readFromNBT(NBTTagCompound tag) {
            NBTTagList chanList = tag.getTagList("channels", 10);

            for (int zoneList = 0; zoneList < chanList.tagCount(); ++zoneList) {
                NBTTagCompound i = chanList.getCompoundTagAt(zoneList);
                String chanTag = i.getString("name");
                Channel chanName = (new Channel(chanTag)).readFromNBT(i);
                ChannelHandler.INSTANCE.addChanel(chanName);
            }

            NBTTagList var8 = tag.getTagList("zones", 10);

            for (int var9 = 0; var9 < var8.tagCount(); ++var9) {
                NBTTagCompound var10 = var8.getCompoundTagAt(var9);
                String var11 = var10.getString("name");
                ChannelZone chan = (new ChannelZone(var11)).readFromNBT(var10);
                ChannelHandler.INSTANCE.addChanel(chan);
            }

            this.markDirty();
        }

        public void writeToNBT(NBTTagCompound tag) {
            NBTTagList chanList = new NBTTagList();
            NBTTagList zoneList = new NBTTagList();
            Iterator i$ = ChannelHandler.INSTANCE.getChannels().keySet().iterator();

            while (i$.hasNext()) {
                String s = (String) i$.next();
                NBTTagCompound chanTag;
                if (!(ChannelHandler.INSTANCE.getChannels().get(s) instanceof ChannelZone)) {
                    chanTag = new NBTTagCompound();
                    ((Channel) ChannelHandler.INSTANCE.getChannels().get(s)).writeToNBT(chanTag);
                    chanList.appendTag(chanTag);
                } else {
                    chanTag = new NBTTagCompound();
                    ((Channel) ChannelHandler.INSTANCE.getChannels().get(s)).writeToNBT(chanTag);
                    zoneList.appendTag(chanTag);
                }
            }

            tag.setTag("channels", chanList);
            tag.setTag("zones", zoneList);
        }

    }
}
