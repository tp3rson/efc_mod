package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil;

import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:59
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class Chat {

    public static String translate(String msg) {
        return StatCollector.canTranslate(msg) ? StatCollector.translateToLocal(msg) : StatCollector.translateToFallback(msg);
    }

    public static void toAll(MinecraftServer server, String msg, Object... parts) {
        server.getConfigurationManager().sendChatMsg(prepareText(msg, parts));
    }

    public static void to(ICommandSender sender, String msg, Object... parts) {
        sender.addChatMessage(prepareText(msg, parts));
    }

    private static IChatComponent prepareText(String msg, Object... parts) {
        String translated = translate(msg);
        String formatted = String.format(translated, parts);
        return new ChatComponentText(formatted);
    }
}