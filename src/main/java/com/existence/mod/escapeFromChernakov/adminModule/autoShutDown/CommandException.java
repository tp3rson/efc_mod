package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil.Chat;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 13:01
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class CommandException extends net.minecraft.command.CommandException {

    public CommandException(String msg, Object... parts) {
        super(Chat.translate(msg), parts);
    }
}