package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:46
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class Config {

    private static final String SCHEDULE = "Schedule";
    private static final String VOTING = "Voting";
    private static final String WATCHDOG = "Watchdog";
    private static final String MESSAGES = "Messages";
    static Configuration config;
    static boolean scheduleEnabled = true;
    static boolean scheduleWarning = true;
    static boolean scheduleDelay = false;
    static boolean scheduleUptime = false;
    static int scheduleHour = 6;
    static int scheduleMinute = 0;
    static int scheduleDelayBy = 5;
    static boolean voteEnabled = true;
    static int voteInterval = 15;
    static int minVoters = 2;
    static int maxNoVotes = 1;
    static boolean watchdogEnabled = false;
    static boolean attemptSoftKill = true;
    static int watchdogInterval = 10;
    static int maxTickTimeout = 40;
    static int lowTPSThreshold = 10;
    static int lowTPSTimeout = 30;
    static String msgWarn = "Server is shutting down in %m minute(s).";
    static String msgKick = "Scheduled server shutdown";

    static void init(File configFile) {
        config = new Configuration(configFile);
        config.setCategoryComment("Schedule", "All times are 24 hour (military) format, relative to machine\'s local time");
        scheduleEnabled = config.getBoolean("Enabled", "Schedule", scheduleEnabled, "If true, server will automatically shutdown");
        scheduleWarning = config.getBoolean("Warnings", "Schedule", scheduleWarning, "If true, server will give five minutes of warnings prior to shutdown");
        scheduleDelay = config.getBoolean("Delay", "Schedule", scheduleDelay, "If true, server will delay shutdown until server is empty");
        scheduleUptime = config.getBoolean("Uptime", "Schedule", scheduleUptime, "If true, server will use Hour and Minute as uptime until shutdown.\nIf false, server will use Hour and Minute as time of day to shutdown.");
        scheduleHour = config.getInt("Hour", "Schedule", scheduleHour, 0, 720, "Hour of the shutdown process (e.g. 8 for 8 AM OR 8 hours uptime)");
        scheduleMinute = config.getInt("Minute", "Schedule", scheduleMinute, 0, 59, "Minute of the shutdown process (e.g. 30 for half-past OR 30 mins uptime)");
        scheduleDelayBy = config.getInt("DelayBy", "Schedule", scheduleDelayBy, 1, 1440, "Minutes to delay scheduled shutdown by, if server is not empty");
        config.setCategoryComment("Voting", "Allows players to shut down the server without admin intervention");
        voteEnabled = config.getBoolean("VoteEnabled", "Voting", voteEnabled, "If true, players may vote to shut server down using \'/shutdown\'");
        voteInterval = config.getInt("VoteInterval", "Voting", voteInterval, 0, 1440, "Min. minutes after a failed vote before new one can begin");
        minVoters = config.getInt("MinVoters", "Voting", minVoters, 1, 999, "Min. players online required to begin a vote");
        maxNoVotes = config.getInt("MaxNoVotes", "Voting", maxNoVotes, 1, 999, "Max. \'No\' votes to cancel a shutdown");
        config.setCategoryComment("Watchdog", "Monitors the server and tries to kill it if unresponsive. USE AT RISK: May corrupt data if killed before or during save");
        watchdogEnabled = config.getBoolean("Enabled", "Watchdog", watchdogEnabled, "If true, try to shutdown the server if unresponsive");
        attemptSoftKill = config.getBoolean("AttemptSoftKill", "Watchdog", attemptSoftKill, "If true, try to save worlds and data before forcing a kill. WARNING: Setting \'false\' is faster, but much higher risk of corruption");
        watchdogInterval = config.getInt("Interval", "Watchdog", watchdogInterval, 1, 3600, "How many seconds between checking for an unresponsive server");
        maxTickTimeout = config.getInt("Timeout", "Watchdog", maxTickTimeout, 1, 3600, "Max. seconds a single server tick may last before killing");
        lowTPSThreshold = config.getInt("LowTPSThreshold", "Watchdog", lowTPSThreshold, 0, 19, "TPS below this value is considered \'too low\'");
        lowTPSTimeout = config.getInt("LowTPSTimeout", "Watchdog", lowTPSTimeout, 1, 3600, "Max. seconds TPS may stay below threshold before killing");
        config.setCategoryComment("Messages", "Customizable messages for the shutdown process");
        msgWarn = config.getString("Warn", "Messages", msgWarn, "Pre-shutdown warning message. Use %m for minutes remaining");
        msgKick = config.getString("Kick", "Messages", msgKick, "Message shown to player on disconnect during shutdown");
        check();
        config.save();
    }

    static void check() {
        if (!scheduleUptime && scheduleHour >= 24) {
            EFCLogger.warn("Uptime shutdown is disabled, but the shutdown hour is more than 23! Please fix this in the config. It will be set to 00 hours.");
            scheduleHour = 0;
        }

        if (scheduleUptime && scheduleHour == 0 && scheduleMinute == 0) {
            EFCLogger.warn("Uptime shutdown is enabled, but is set to shutdown after 0 hours and 0 minutes of uptime! Please fix this in the config. It will be set to 24 hours.");
            scheduleHour = 24;
        }

    }

    static boolean isNothingEnabled() {
        return !scheduleEnabled && !voteEnabled && !watchdogEnabled;
    }
}
