package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:41
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */

/**
 * @Mod( modid = "AutoShutdown",
 * name = "AutoShutdown",
 * version = "0.0.3c",
 * acceptableRemoteVersions = "*",
 * acceptableSaveVersions = ""
 * )
 */
public class ASReference {

    public static final String MOD_ID = "autoshutdown";
    public static final String MOD_NAME = "AutoShutdown Mod for safe server restarting";
    public static final String VERSION = "@VERSION@";
    private static final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    public static final String VERSION_DATE = dateFormat.format(new Date());
}
