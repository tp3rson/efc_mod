package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:40
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
@Mod(
        modid = ASReference.MOD_ID,
        name = ASReference.MOD_NAME,
        version = ASReference.VERSION,
        acceptableRemoteVersions = "*",
        acceptableSaveVersions = "")
public class AutoShutdownMain {

    @Mod.EventHandler
    @SideOnly(Side.CLIENT)
    public void clientPreInit(FMLPreInitializationEvent event) {
        EFCLogger.error("This mod is intended only for use on servers.");
        EFCLogger.error("Please consider removing this mod from your installation.");
    }

    @Mod.EventHandler
    @SideOnly(Side.SERVER)
    public void serverPreInit(FMLPreInitializationEvent event) {
        Config.init(event.getSuggestedConfigurationFile());
    }

    @Mod.EventHandler
    @SideOnly(Side.SERVER)
    public void serverStart(FMLServerStartingEvent event) {
        if (Config.isNothingEnabled()) {
            EFCLogger.warn("It appears no ForgeAutoShutdown features are enabled.");
            EFCLogger.warn("Please check the config at `config/AutoShutdown.cfg`.");
        } else {
            if (Config.scheduleEnabled) {
                ShutdownTask.create();
            }

            if (Config.voteEnabled) {
                ShutdownCommand.create(event);
            }

            if (Config.watchdogEnabled) {
                WatchdogTask.create();
            }

        }
    }
}
