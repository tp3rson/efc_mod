package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil.Chat;
import com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil.Server;
import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

import java.util.*;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 13:00
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class ShutdownCommand implements ICommand {

    static final List ALIASES = Collections.singletonList("shutdown");
    static final List OPTIONS = Arrays.asList(new String[]{"yes", "no"});
    private static ShutdownCommand INSTANCE;
    private static MinecraftServer SERVER;
    HashMap votes = new HashMap();
    Date lastVote = new Date(0L);
    boolean voting = false;


    public static void create(FMLServerStartingEvent event) {
        if (INSTANCE != null) {
            throw new RuntimeException("ShutdownCommand can only be created once");
        } else {
            INSTANCE = new ShutdownCommand();
            SERVER = MinecraftServer.getServer();
            event.registerServerCommand(INSTANCE);
            EFCLogger.debug("`/shutdown` command registered");
        }
    }

    public void processCommand(ICommandSender sender, String[] args) {
        if (sender == SERVER) {
            throw new CommandException("FAS.error.playersonly", new Object[0]);
        } else {
            if (this.voting) {
                this.processVote(sender, args);
            } else {
                this.initiateVote(args);
            }

        }
    }

    private void initiateVote(String[] args) {
        if (args.length >= 1) {
            throw new CommandException("FAS.error.novoteinprogress", new Object[0]);
        } else {
            Date now = new Date();
            long interval = (long) (Config.voteInterval * 60 * 1000);
            long difference = now.getTime() - this.lastVote.getTime();
            if (difference < interval) {
                throw new CommandException("FAS.error.toosoon", new Object[]{Long.valueOf((interval - difference) / 1000L)});
            } else {
                List players = SERVER.getConfigurationManager().playerEntityList;
                if (players.size() < Config.minVoters) {
                    throw new CommandException("FAS.error.notenoughplayers", new Object[]{Integer.valueOf(Config.minVoters)});
                } else {
                    Chat.toAll(SERVER, "FAS.msg.votebegun", new Object[0]);
                    this.voting = true;
                }
            }
        }
    }

    private void processVote(ICommandSender sender, String[] args) {
        if (args.length < 1) {
            throw new CommandException("FAS.error.voteinprogress", new Object[0]);
        } else if (!OPTIONS.contains(args[0].toLowerCase())) {
            throw new CommandException("FAS.error.votebadsyntax", new Object[0]);
        } else {
            String name = sender.getCommandSenderName();
            Boolean vote = Boolean.valueOf(args[0].equalsIgnoreCase("yes"));
            if (this.votes.containsKey(name)) {
                Chat.to(sender, "FAS.msg.votecleared", new Object[0]);
            }

            this.votes.put(name, vote);
            Chat.to(sender, "FAS.msg.voterecorded", new Object[0]);
            this.checkVotes();
        }
    }

    private void checkVotes() {
        int players = SERVER.getConfigurationManager().playerEntityList.size();
        if (players < Config.minVoters) {
            this.voteFailure("FAS.fail.notenoughplayers");
        } else {
            int yes = Collections.frequency(this.votes.values(), Boolean.valueOf(true));
            int no = Collections.frequency(this.votes.values(), Boolean.valueOf(false));
            if (no >= Config.maxNoVotes) {
                this.voteFailure("FAS.fail.maxnovotes");
            } else {
                if (yes + no == players) {
                    this.voteSuccess();
                }

            }
        }
    }

    private void voteSuccess() {
        EFCLogger.info("Server shutdown initiated by vote");
        Server.shutdown("FAS.msg.usershutdown");
    }

    private void voteFailure(String reason) {
        Chat.toAll(SERVER, reason, new Object[0]);
        this.votes.clear();
        this.lastVote = new Date();
        this.voting = false;
    }

    public String getCommandName() {
        return "shutdown";
    }

    public String getCommandUsage(ICommandSender sender) {
        return "/shutdown <yes|no>";
    }

    public List getCommandAliases() {
        return ALIASES;
    }

    public boolean canCommandSenderUseCommand(ICommandSender sender) {
        return Config.voteEnabled;
    }

    public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        return OPTIONS;
    }

    public boolean isUsernameIndex(String[] args, int idx) {
        return false;
    }

    public int compareTo(Object o) {
        ICommand command = (ICommand) o;
        return command.getCommandName().compareTo(this.getCommandName());
    }
}