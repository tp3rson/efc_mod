package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil;

import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.util.FakePlayer;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:55
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class Server {

    static final MinecraftServer SERVER = MinecraftServer.getServer();


    public static void shutdown(String reason) {
        reason = Chat.translate(reason);
        Object[] var1 = SERVER.getConfigurationManager().playerEntityList.toArray();
        int var2 = var1.length;

        for (int var3 = 0; var3 < var2; ++var3) {
            Object value = var1[var3];
            EntityPlayerMP player = (EntityPlayerMP) value;
            player.playerNetServerHandler.kickPlayerFromServer(reason);
        }

        EFCLogger.debug("Shutdown initiated because: %s", new Object[]{reason});
        SERVER.initiateShutdown();
    }

    public static boolean hasRealPlayers() {
        Object[] var0 = SERVER.getConfigurationManager().playerEntityList.toArray();
        int var1 = var0.length;

        for (int var2 = 0; var2 < var1; ++var2) {
            Object value = var0[var2];
            if (value instanceof EntityPlayerMP && !(value instanceof FakePlayer)) {
                return true;
            }
        }

        return false;
    }
}
