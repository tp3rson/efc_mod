package com.existence.mod.escapeFromChernakov.adminModule.autoShutDown;

import com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil.Chat;
import com.existence.mod.escapeFromChernakov.adminModule.autoShutDown.internalUtil.Server;
import com.existence.mod.escapeFromChernakov.util.EFCLogger;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.server.MinecraftServer;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Not for free use!
 *
 * @author : Daniel (tp3rson)
 * Date: 11.05.2018
 * Time: 12:53
 * All rights reserved by Existence Team!
 * https://vk.com/existenceservers
 */
public class ShutdownTask extends TimerTask {

    static final Format DATE = new SimpleDateFormat("HH:mm MMM d");
    private static ShutdownTask INSTANCE;
    private static MinecraftServer SERVER;
    boolean executeTick = false;
    byte warningsLeft = 5;
    int delayMinutes = 0;


    public static void create() {
        if (INSTANCE != null) {
            throw new RuntimeException("ShutdownTask can only be created once");
        } else {
            INSTANCE = new ShutdownTask();
            SERVER = MinecraftServer.getServer();
            Timer timer = new Timer("ForgeAutoShutdown timer");
            Calendar shutdownAt = Calendar.getInstance();
            if (Config.scheduleUptime) {
                shutdownAt.add(11, Config.scheduleHour);
                shutdownAt.add(12, Config.scheduleMinute);
            } else {
                shutdownAt.set(11, Config.scheduleHour);
                shutdownAt.set(12, Config.scheduleMinute);
                shutdownAt.set(13, 0);
                if (shutdownAt.before(Calendar.getInstance())) {
                    shutdownAt.add(5, 1);
                }
            }

            Date shutdownAtDate = shutdownAt.getTime();
            timer.schedule(INSTANCE, shutdownAtDate, 60000L);
            EFCLogger.info("Next automatic shutdown: %s", new Object[]{DATE.format(shutdownAtDate)});
        }
    }

    public void run() {
        FMLCommonHandler.instance().bus().register(this);
        this.executeTick = true;
        EFCLogger.debug("Timer called; next ShutdownTask tick will run");
    }

    @SubscribeEvent(
            priority = EventPriority.HIGHEST
    )
    public void onServerTick(TickEvent.ServerTickEvent event) {
        if (this.executeTick && event.phase != TickEvent.Phase.END) {
            this.executeTick = false;
            if (Config.scheduleDelay && this.performDelay()) {
                EFCLogger.debug("ShutdownTask ticked; %d minute(s) of delay to go", new Object[]{Integer.valueOf(this.delayMinutes)});
                --this.delayMinutes;
            } else {
                if (Config.scheduleWarning && this.warningsLeft > 0) {
                    this.performWarning();
                    EFCLogger.debug("ShutdownTask ticked; %d warning(s) to go", new Object[]{Byte.valueOf(this.warningsLeft)});
                } else {
                    Server.shutdown(Config.msgKick);
                }

            }
        }
    }

    private boolean performDelay() {
        if (this.delayMinutes > 0) {
            return true;
        } else if (!Server.hasRealPlayers()) {
            return false;
        } else {
            this.warningsLeft = 5;
            this.delayMinutes += Config.scheduleDelayBy;
            EFCLogger.info("Shutdown delayed by %d minutes; server is not empty", new Object[]{Integer.valueOf(this.delayMinutes)});
            return true;
        }
    }

    private void performWarning() {
        String warning = Config.msgWarn.replace("%m", Byte.toString(this.warningsLeft));
        Chat.toAll(SERVER, "*** " + warning, new Object[0]);
        EFCLogger.info(warning);
        --this.warningsLeft;
    }
}