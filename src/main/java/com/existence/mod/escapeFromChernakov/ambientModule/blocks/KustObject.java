package com.existence.mod.escapeFromChernakov.ambientModule.blocks;

import net.minecraft.util.IIcon;

public class KustObject {
    public String iconname;
    //public float[] sizes;
    //public float[] iSizes;
    public String name;
    public IIcon icon;
    boolean recalc = false;

    public KustObject(String iconname, String name) {
        this.iconname = iconname;
        //this.sizes = sizes;
        this.name = name;
        //this.iSizes = is;
    }

    //public float[] getSizes() {
    //    return this.sizes;
    //}

    public IIcon getIcon() {
        if (!this.recalc) {
            this.recalcIcon();
        }
        return this.icon;
    }

    public void setIcon(IIcon i) {
        this.icon = i;
    }

    public void recalcIcon() {
        //this.recalc = true;
        //this.icon = new KustIcon(this.icon, this.iSizes[0], this.iSizes[1]);
    }

    public static class KustIcon
            implements IIcon {
        private IIcon base;
        private float uMod = 0.0f;
        private float vMod = 0.0f;

        public KustIcon(IIcon base, float uMod, float vMod) {
            this.base = base;
            this.uMod = (this.getMaxU() - this.getMinU()) * uMod;
            this.vMod = (this.getMaxV() - this.getMinV()) * vMod;
        }

        public int getIconWidth() {
            return this.base.getIconWidth();
        }

        public int getIconHeight() {
            return this.base.getIconHeight();
        }

        public float getMinU() {
            return this.base.getMinU();
        }

        public float getMaxU() {
            return this.base.getMaxU() - this.uMod;
        }

        public float getInterpolatedU(double par1) {
            float f = this.getMaxU() - this.getMinU();
            return this.getMinU() + f * ((float) par1 / 16.0f);
        }

        public float getMinV() {
            return this.base.getMinV();
        }

        public float getMaxV() {
            return this.base.getMaxV() - this.vMod;
        }

        public float getInterpolatedV(double par1) {
            float f = this.getMaxV() - this.getMinV();
            return this.getMinV() + f * ((float) par1 / 16.0f);
        }

        public String getIconName() {
            return this.base.getIconName();
        }
    }

}
