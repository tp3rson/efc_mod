package com.existence.mod.escapeFromChernakov.ambientModule.bushes;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import org.lwjgl.input.Keyboard;

@Mod(modid = "kusti", name = "Kusti", version = "1.1.0")
public class Main {
    @Mod.Instance(value = "kusti")
    public static Main instance;

    public static KeyBinding[] keyBindings;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {

        int i = 0;
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 1.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 2.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.0f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 3.0f, 1.0f}, "Куст " + i, new float[]{0.67f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 1.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 2.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.0f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 3.0f, 1.0f}, "Куст " + i, new float[]{0.67f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 1.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 2.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.0f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 3.0f, 1.0f}, "Куст " + i, new float[]{0.67f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 1.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 2.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.0f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 3.0f, 1.0f}, "Куст " + i, new float[]{0.67f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 1.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 2.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.0f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 3.0f, 1.0f}, "Куст " + i, new float[]{0.67f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 1.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.5f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 2.0f, 1.0f}, "Куст " + i, new float[]{0.0f, 0.0f}));
        BlockCustomKust.kustRegistry.add(new KustObject("kusti:KUST" + i++, new float[]{1.0f, 3.0f, 1.0f}, "Куст " + i, new float[]{0.67f, 0.5f}));
        Block.getBlockFromName((String) "tallgrass").setBlockBounds(0.0f, 0.0f, 0.0f, 1.0f, 0.012f, 1.0f);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        GameRegistry.registerBlock((Block) new BlockCustomKust(), KustItemBlock.class, (String) "kust");
        KustProxy.instance.registerTileRenders();

        FMLCommonHandler.instance().bus().register(this);

        keyBindings = new KeyBinding[2];


        keyBindings[0] = new KeyBinding("key.structure.desc", Keyboard.KEY_P, "key.magicbeans.category");
        keyBindings[1] = new KeyBinding("key.hud.desc", Keyboard.KEY_H, "key.magicbeans.category");

// Регистрируем
        for (int i = 0; i < keyBindings.length; ++i) {
            ClientRegistry.registerKeyBinding(keyBindings[i]);
        }
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public void onEvent(InputEvent.KeyInputEvent event) {
        // Проверка, сработает при нажатии любой клавиши
        //System.out.println("Key Input Event");

        // Достаём массив
        KeyBinding[] keyBindings = Main.keyBindings;//Main - главный класс

        // Проверяем на нужную кнопку
        if (keyBindings[0].isPressed()) {
            EntityPlayer world = Minecraft.getMinecraft().thePlayer;
            // Выводит сообщение при нажатии клавиши в ячейке 0(можно делать всё что угодно)
            //System.out.println("Key binding ="+keyBindings[0].getKeyDescription());
            world.playSound("mob.zombie.death", 1.0F, 1.0F);
            EntityZombie zonbie;
        }
        if (keyBindings[1].isPressed()) {
            // Выводит сообщение при нажатии клавиши в ячейке 1(можно делать всё что угодно)
            //System.out.println("Key binding ="+keyBindings[1].getKeyDescription());


        }
    }
}

