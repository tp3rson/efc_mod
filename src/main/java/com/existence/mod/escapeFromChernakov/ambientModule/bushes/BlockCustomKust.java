package com.existence.mod.escapeFromChernakov.ambientModule.bushes;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class BlockCustomKust
        extends Block {
    public static List<KustObject> kustRegistry = new ArrayList<KustObject>();

    public BlockCustomKust() {
        super(Material.circuits);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setBlockBounds(0.0f, 0.0f, 0.0f, 1.0f, 0.012f, 1.0f);
    }

    public static KustObject getFromRegistry(int md) {
        return kustRegistry.get(md);
    }

    public void getSubBlocks(Item item, CreativeTabs tab, List list) {
        for (int i = 0; i < kustRegistry.size(); ++i) {
            list.add(new ItemStack(item, 1, i));
        }
    }

    public int damageDropped(int par1) {
        return par1;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
        return null;
    }

    public boolean isOpaqueCube() {
        return false;
    }

    public void registerBlockIcons(IIconRegister iconRegister) {
        for (KustObject ko : kustRegistry) {
            ko.setIcon(iconRegister.registerIcon(ko.iconname));
        }
    }

    public boolean canBlockStay(World par1World, int x, int y, int z) {
        String block = par1World.getBlock(x, y - 1, z).getUnlocalizedName();
        return "tile.dirt".equals(block) || "tile.grass".equals(block);
    }

    public void onNeighborBlockChange(World par1World, int par2, int par3, int par4, Block b) {
        super.onNeighborBlockChange(par1World, par2, par3, par4, b);
        if (!this.canBlockStay(par1World, par2, par3, par4)) {
            par1World.setBlockToAir(par2, par3, par4);
        }
    }

    public IIcon getIcon(int par1, int par2) {
        return kustRegistry.get(par2).getIcon();
    }

    public int getRenderType() {
        return KustProxy.kustRenderId;
    }
}
