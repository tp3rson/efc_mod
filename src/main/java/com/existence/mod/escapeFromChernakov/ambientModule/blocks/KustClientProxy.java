package com.existence.mod.escapeFromChernakov.ambientModule.blocks;

import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraft.client.settings.KeyBinding;
import org.lwjgl.input.Keyboard;

public class KustClientProxy
        extends KustProxy {

    public static KeyBinding[] keyBindings;

    @Override
    public void registerTileRenders() {
        ///kustRenderId = RenderingRegistry.getNextAvailableRenderId();
        //RenderingRegistry.registerBlockHandler((ISimpleBlockRenderingHandler) new KustRenderer());
    }

    public void initKeyHandlers() {
        keyBindings = new KeyBinding[2];

// instantiate the key bindings
        keyBindings[0] = new KeyBinding("key.structure.desc", Keyboard.KEY_NUMPAD0, "key.magicbeans.category");
        keyBindings[1] = new KeyBinding("key.hud.desc", Keyboard.KEY_NUMPAD1, "key.magicbeans.category");

// register all the key bindings
        for (int i = 0; i < keyBindings.length; ++i) {
            ClientRegistry.registerKeyBinding(keyBindings[i]);
        }
    }
}
